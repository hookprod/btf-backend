package controllers;

import play.mvc.Http;
import play.mvc.Security;

/**
 * Created by Gleb Zykov on 15/01/2018.
 */
public class BtrAuthenticator extends Security.Authenticator {

    public static final String KEY_USER_ID = "userId";

    //1cd1d07d-2202-4b35-b530-962159059287
    @Override
    public String getUsername(Http.Context ctx) {
        return ctx.session().get(KEY_USER_ID);
    }

}
