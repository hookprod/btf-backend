package controllers.registration;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Gleb Zykov on 26/01/2018.
 */
public class RecoverPasswordRequest {

    @NotEmpty
    public String recoveryToken;

    @NotEmpty
    public String password;

}
