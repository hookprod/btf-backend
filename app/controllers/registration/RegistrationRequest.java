package controllers.registration;

import model.UserProfile;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.mindrot.jbcrypt.BCrypt;

public class RegistrationRequest {

    @NotBlank
    private String password;

    @Email
    @NotBlank
    private String email;
    
    private String firstName;
    private String lastName;
    private String referralCode;

    public UserProfile toUserProfile() {
        UserProfile userProfile = new UserProfile();
        userProfile.setPasswordHash(BCrypt.hashpw(password, BCrypt.gensalt()));
        userProfile.setEmail(email);
        userProfile.setFirstName(firstName);
        userProfile.setLastName(lastName);
        return userProfile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }
}
