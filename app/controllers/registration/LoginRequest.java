package controllers.registration;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Gleb Zykov on 15/01/2018.
 */
public class LoginRequest {

    @NotEmpty
    public String email;

    @NotEmpty
    public String password;

}
