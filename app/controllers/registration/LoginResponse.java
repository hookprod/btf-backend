package controllers.registration;

/**
 * Created by Gleb Zykov on 23/01/2018.
 */
public class LoginResponse {

    public final String authorizationToken;

    public LoginResponse(String authorizationToken) {
        this.authorizationToken = authorizationToken;
    }

}
