package controllers.registration;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Gleb Zykov on 26/01/2018.
 */
public class PasswordRecoveryRequest {

    @Email
    @NotEmpty
    public String email;

}
