package controllers.registration;

import controllers.ApiController;
import controllers.AuthenticatedAction;
import controllers.BtrAuthenticator;
import controllers.ErrorResponse;
import model.UserProfile;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Security;
import services.JwtService;
import services.RegistrationService;
import services.UserProfileService;

import javax.inject.Inject;
import java.util.Optional;

public class RegistrationController extends ApiController {

    private final RegistrationService registrationService;
    private final UserProfileService userProfileService;

    @Inject
    public RegistrationController(UserProfileService userProfileService, RegistrationService registrationService) {
        this.registrationService = registrationService;
        this.userProfileService = userProfileService;
    }

    public Result register() {
        RegistrationRequest registrationRequest = parseBodyAs(RegistrationRequest.class);
        registrationService.newRegistration(registrationRequest);
        return okResponse(registrationRequest);
    }

    public Result verifyEmail(String token) {
        try {
            registrationService.confirmRegistration(token);
            return redirect("https://crowdsale.bitrust.co.uk/login");
        } catch (IllegalArgumentException iae) {
            return redirect("https://crowdsale.bitrust.co.uk/login");
        }
    }

    public Result login() {
        LoginRequest loginRequest = parseBodyAs(LoginRequest.class);
        try {
            UserProfile userProfile = registrationService.checkPassword(loginRequest.email, loginRequest.password);
            return okResponse(new LoginResponse(JwtService.generateJwt(AuthenticatedAction.SECRET,
                    userProfile.getId())));
        } catch (IllegalAccessException e) {
            return unauthorized(Json.toJson(new ErrorResponse(e.getMessage())));
        }
    }

    public Result authenticatedRequest() {
        return ok("you are authenticated");
    }

    public Result requestPasswordRecovery() {
        PasswordRecoveryRequest passwordRecoveryRequest = parseBodyAs(PasswordRecoveryRequest.class);
        Optional<UserProfile> userProfile = userProfileService.findByEmail(passwordRecoveryRequest.email);
        if(!userProfile.isPresent()) {
            return badRequestResponse("no email registered");
        }
        registrationService.recoverPassword(userProfile.get().getId());
        return noContent();
    }

    public Result setNewPassword() {
        RecoverPasswordRequest recoverPasswordRequest = parseBodyAs(RecoverPasswordRequest.class);
        registrationService.changePassword(recoverPasswordRequest.password, recoverPasswordRequest.recoveryToken);
        return noContent();
    }


}
