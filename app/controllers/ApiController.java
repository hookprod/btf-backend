package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.stream.Collectors;

@With(ApiAction.class)
public abstract class ApiController extends Controller {

    private ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    protected int getPage() {
        String pageStr = request().getQueryString("page");
        if(pageStr == null || pageStr.isEmpty()) {
            throw new IllegalArgumentException("no page parameter");
        }
        try {
            return Integer.parseInt(pageStr);
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException("no page parameter");
        }
    }

    protected int getSize() {
        String sizeStr = request().getQueryString("size");
        if(sizeStr == null || sizeStr.isEmpty()) {
            throw new IllegalArgumentException("no size parameter");
        }
        try {
            return Integer.parseInt(sizeStr);
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException("no page parameter");
        }
    }

    public void addTotalCountHeader(long totalCount) {
        response().setHeader("X-Total-Count", String.valueOf(totalCount));
    }

    protected <T> T parseBodyAs(Class<T> aClass) {
        JsonNode json = request().body().asJson();
        if (json == null) {
            throw new IllegalArgumentException("no json data");
        }
        T valueObject;
        try {
            valueObject = Json.fromJson(json, aClass);
        } catch (Exception ex) {
            Logger.warn("exception in {} request from {}:\n\tcould not parse json value {} to {}, exception -\n\t\t{}",
                    request().uri(), request().remoteAddress(), json.toString(), aClass, ex.getMessage());
            throw new IllegalArgumentException("could not parse json to entity, exception is: " + ex.getMessage());
        }

        //validate
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(valueObject)
                .stream()
                .filter(constraintViolation -> !constraintViolation.getPropertyPath().toString().equals("taxiStation"))
                .collect(Collectors.toSet());

        if (!constraintViolations.isEmpty()) {
            String violationsText = constraintViolations.stream()
                    .map(violation -> violation.getPropertyPath().toString() + " " + violation.getMessage())
                    .reduce("", (a, b) -> a.concat(b).concat("; "))
                    .trim();
            Logger.warn("not valid request {} from {}:\n\t\t{}", request().uri(), request().remoteAddress(),
                    violationsText);
            throw new IllegalArgumentException(violationsText);
        }
        return valueObject;
    }

    public Result resultResponse(String text) {
        return okResponse(new ResultResponse(text));
    }

    public Result okResponse(Object value) {
        return ok(Json.toJson(value));
    }

    public Result createdResponse(Object value) {
        return created(Json.toJson(value));
    }

    public Result badRequestResponse(String value) {
        return badRequest(Json.toJson(new ErrorResponse(value)));
    }

    public Result notFoundResponse(String message) {
        return notFound(Json.toJson(new ErrorResponse(message)));
    }

    public Result unauthorizedResponse(String message) {
        return unauthorized(Json.toJson(new ErrorResponse(message)));
    }

    public String getUserId() {
        return AuthenticatedAction.getUserId(ctx());
    }

}
