package controllers;

import play.Logger;
import play.libs.F;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import scala.concurrent.Promise;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class ApiAction extends play.mvc.Action.Simple {
    @Override
    public CompletionStage<Result> call(Http.Context ctx) {
        try {
            return delegate.call(ctx);
        } catch (IllegalArgumentException iae) {
            return CompletableFuture.completedFuture(
                    badRequest(Json.toJson(new ErrorResponse(iae.getMessage()))));
        } catch (Exception ex) {
            if(ex.getCause() != null && ex.getCause() instanceof IllegalArgumentException) {
                return CompletableFuture.completedFuture(
                        badRequest(Json.toJson(new ErrorResponse(ex.getCause().getMessage()))));
            }
            Logger.error("exception while handing api request", ex);
            return CompletableFuture.completedFuture(
                    internalServerError(Json.toJson(new ErrorResponse(ex.getMessage()))));
        }
    }
}
