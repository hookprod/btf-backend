package controllers.profile;

import org.hibernate.validator.constraints.NotBlank;

public class SetErc20WalletAddressRequest {

    @NotBlank
    public String erc20walletAddress;

}
