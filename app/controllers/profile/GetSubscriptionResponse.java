package controllers.profile;

/**
 * Created by Gleb Zykov on 26/01/2018.
 */
public class GetSubscriptionResponse {

    public final boolean subscribed;

    public GetSubscriptionResponse(boolean subscribed) {
        this.subscribed = subscribed;
    }
}
