package controllers.profile;

import model.UserProfile;
import services.BonusService;

import java.math.BigInteger;

/**
 * Created by Gleb Zykov on 26/01/2018.
 * entity for user responses
 */
public class ReferralResponse {

    public final String email;

    public final BigInteger reward;

    public final Long registrationTime;

    public ReferralResponse(UserProfile userProfile) {
        this.email = userProfile.getEmail();
        this.reward = userProfile.getTokensBought()
                .multiply(BigInteger.valueOf(BonusService.REFERRAL_BONUS_PERCENT))
                .divide(BigInteger.valueOf(100));
        if(userProfile.getRegistrationTime() == null) {
            this.registrationTime = 0L;
        } else {
            this.registrationTime = userProfile.getRegistrationTime() / 1000;
        }
    }
}
