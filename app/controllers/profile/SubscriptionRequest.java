package controllers.profile;

import javax.validation.constraints.NotNull;

public class SubscriptionRequest {

    @NotNull
    public Boolean subscribe;

}
