package controllers.profile;

import model.UserProfile;

import java.math.BigInteger;

/**
 * Created by Gleb Zykov on 30/01/2018.
 * profile answer adapter
 */
public class ProfileResponse {

    public final  String email;
    public final String firstName;
    public final String lastName;
    public final String referralCode;
    public final String phoneNumber;

    public final String ethAddress;
    public final String btcAddress;
    public final String ltcAddress;

    public final long registrationTime;

    public final BigInteger tokensBought;
    public final BigInteger bonusTokens;
    public final BigInteger referralTokens;
    public final BigInteger bountyTokens;

    public final BigInteger ethInvested;
    public final BigInteger btcInvested;
    public final BigInteger ltcInvested;

    public final BigInteger bountyAndReferralTokens;

    public final  String erc20WalletAddress;

    public ProfileResponse(UserProfile userProfile) {
        this.email =userProfile.getEmail();
        this.firstName = userProfile.getFirstName();
        this.lastName = userProfile.getLastName();
        this.referralCode = userProfile.getReferralCode();
        this.phoneNumber = userProfile.getPhoneNumber();
        this.ethAddress = userProfile.getEthAddress();
        this.btcAddress = userProfile.getBtcAddress();
        this.ltcAddress = userProfile.getLtcAddress();

        if(userProfile.getRegistrationTime() == null) {
            this.registrationTime = 0;
        } else {
            this.registrationTime = userProfile.getRegistrationTime() / 1000;
        }

        if(userProfile.getTokensBought() == null) {
            this.tokensBought = BigInteger.ZERO;
        } else {
            this.tokensBought = userProfile.getTokensBought();
        }

        if(userProfile.getBonusTokens() == null) {
            this.bonusTokens = BigInteger.ZERO;
        } else {
            this.bonusTokens = userProfile.getBonusTokens();
        }

        if(userProfile.getReferralTokens() == null) {
            this.referralTokens = BigInteger.ZERO;
        } else {
            this.referralTokens = userProfile.getReferralTokens();
        }

        if(userProfile.getBountyTokens() == null) {
            this.bountyTokens = BigInteger.ZERO;
        } else {
            this.bountyTokens = userProfile.getBountyTokens();
        }

        if(userProfile.getEthInvested() == null) {
            this.ethInvested = BigInteger.ZERO;
        } else {
            this.ethInvested = userProfile.getEthInvested();
        }

        if(userProfile.getBtcInvested() == null) {
            this.btcInvested = BigInteger.ZERO;
        } else {
            this.btcInvested = userProfile.getBtcInvested();
        }

        if(userProfile.getLtcInvested() == null) {
            this.ltcInvested = BigInteger.ZERO;
        } else {
            this.ltcInvested = userProfile.getLtcInvested();
        }

        this.bountyAndReferralTokens = this.bountyTokens.add(this.referralTokens);

        this.erc20WalletAddress = userProfile.getErc20WalletAddress();
    }

}
