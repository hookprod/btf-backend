package controllers.profile;

import controllers.ApiController;
import controllers.AuthenticatedAction;
import controllers.BtrAuthenticator;
import controllers.ErrorResponse;
import controllers.bonuses.SharingRecordAnswer;
import model.PurchaseTransaction;
import model.SharingType;
import model.UserProfile;
import model.UserSharingRecord;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import services.BonusService;
import services.PurchaseTransactionsService;
import services.SubscriptionService;
import services.UserProfileService;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@With(AuthenticatedAction.class)
public class UserProfileController extends ApiController {

    private final UserProfileService userProfileService;
    private final PurchaseTransactionsService purchaseTransactionsService;
    private final SubscriptionService subscriptionService;

    @Inject
    public UserProfileController(UserProfileService userProfileService,
                                 PurchaseTransactionsService purchaseTransactionsService,
                                 SubscriptionService subscriptionService) {
        this.userProfileService = userProfileService;
        this.purchaseTransactionsService = purchaseTransactionsService;
        this.subscriptionService = subscriptionService;
    }

    public Result getProfile() {
        String userId = getUserId();
        Optional<UserProfile> userProfileOptional = userProfileService.findById(userId);
        if(!userProfileOptional.isPresent()) {
            return unauthorizedResponse("user not found");
        }
        UserProfile userProfile = userProfileOptional.get();
        return okResponse(new ProfileResponse(userProfile));
    }

    public Result getReferrals() {
        String userId = getUserId();
        int page = getPage();
        int size = getSize();
        List<UserProfile> referrals = userProfileService.getReferrals(userId, page, size);

        List<ReferralResponse> referralResponses = referrals.stream()
                .map(ReferralResponse::new)
                .collect(Collectors.toList());

        addTotalCountHeader(userProfileService.getReferralsCount(userId));
        return okResponse(referralResponses);
    }

    public Result getSubscription() {
        String userId = getUserId();

        Optional<UserProfile> profile = userProfileService.findById(userId);
        if(!profile.isPresent()) {
            return unauthorizedResponse("please reauthorize - user not found");
        }
        String email = profile.get().getEmail();
        boolean subscribed = subscriptionService.isSubscribed(email);
        return okResponse(new GetSubscriptionResponse(subscribed));
    }

    public Result editProfile() {
        EditProfileRequest editProfileRequest = parseBodyAs(EditProfileRequest.class);
        String userId = getUserId();
        UserProfile userProfile = userProfileService.editUser(userId, editProfileRequest);
        return okResponse(userProfile);
    }

    public Result setWalletAddress() {
        String userId = getUserId();
        Optional<UserProfile> userProfileOptional = userProfileService.findById(userId);

        if(!userProfileOptional.isPresent()) {
            return unauthorizedResponse("user not found");
        }

        SetErc20WalletAddressRequest setErc20WalletAddressRequest = parseBodyAs(SetErc20WalletAddressRequest.class);
        userProfileService.setWalletAddress(userId, setErc20WalletAddressRequest.erc20walletAddress);
        return noContent();
    }

    public Result subscribe() {
        String userId = getUserId();
        Optional<UserProfile> userProfileOptional = userProfileService.findById(userId);
        if(!userProfileOptional.isPresent()) {
            return unauthorizedResponse("user not found");
        }
        subscriptionService.subscribeEmail(userProfileOptional.get().getEmail());
        String text = "You were successfully subscribed";
        return okResponse(new SubscriptionResponse(text));
    }

    public Result unsubscribe() {
        String userId = getUserId();
        Optional<UserProfile> userProfileOptional = userProfileService.findById(userId);
        if(!userProfileOptional.isPresent()) {
            return unauthorizedResponse("user not found");
        }
        subscriptionService.unsubscribeEmail(userProfileOptional.get().getEmail());
        String text = "You were successfully unsubscribed";
        return okResponse(new SubscriptionResponse(text));
    }

    public Result getTransactions() {
        String userId = getUserId();
        int page = getPage();
        int size = getSize();
        addTotalCountHeader(purchaseTransactionsService.getTransactionsCount(getUserId()));
        List<PurchaseTransaction> transactions = purchaseTransactionsService.getTransactions(userId, page, size);
        return okResponse(transactions);
    }

}
