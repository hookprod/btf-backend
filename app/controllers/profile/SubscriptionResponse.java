package controllers.profile;

public class SubscriptionResponse {

    public final String text;

    public SubscriptionResponse(String text) {
        this.text = text;
    }

}
