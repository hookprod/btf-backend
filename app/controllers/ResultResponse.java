package controllers;

/**
 * Created by Gleb Zykov on 19/01/2018.
 */
public class ResultResponse {

    public final String text;

    public ResultResponse(String text) {
        this.text = text;
    }
}
