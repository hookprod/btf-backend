package controllers;

import play.mvc.*;

import services.CheckerService;
import services.blockhain.BitcoinBlockchainService;
import services.blockhain.transactionsChecker.SchedulerService;
import services.blockhain.ethereum.EthereumBlockchainService;
import services.blockhain.LitecoinBlockchainService;
import services.clearing.ClearingService;

import javax.inject.Inject;
import java.io.IOException;
import java.math.BigInteger;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    private final EthereumBlockchainService ethereumBlockchainService;
    private final LitecoinBlockchainService litecoinBlockchainService;
    private final BitcoinBlockchainService bitcoinBlockchainService;
    private final CheckerService checkerService;
    private final ClearingService clearingService;

    @Inject
    public HomeController(EthereumBlockchainService ethereumBlockchainService,
                          SchedulerService ccs,
                          BitcoinBlockchainService bitcoinBlockchainService,
                          LitecoinBlockchainService litecoinBlockchainService,
                          ClearingService clearingService,
                          CheckerService checkerService) {
        this.ethereumBlockchainService = ethereumBlockchainService;
        this.litecoinBlockchainService = litecoinBlockchainService;
        this.bitcoinBlockchainService = bitcoinBlockchainService;
        this.clearingService = clearingService;
        this.checkerService = checkerService;
    }

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() throws IOException {
        return ok("server is running");
    }

    public Result testTransaction(String to, Long amount) {
        String result = ethereumBlockchainService.sendTokens(to, BigInteger.valueOf(amount));
        return ok("result is: " + result);
    }

    public Result testTransactionStatus(String transactionHash) {
        boolean successful = ethereumBlockchainService.isTransactionSuccessfulAndConfirmed(transactionHash);
        return ok("success: " + successful);
    }

    public Result runClearing() {
        clearingService.runClearing();
        return ok("clearing is done");
    }

    public Result status() {

        boolean btcConnection = false;
        boolean ltcConnection = false;
        boolean gethConnection = false;

        try {
            bitcoinBlockchainService.listTransactions(0, 1);
            btcConnection = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            litecoinBlockchainService.listTransactions(0, 1);
            ltcConnection = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            long blockNumber = ethereumBlockchainService.getBlockNumber();
            gethConnection = blockNumber > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        boolean isBonusOk = checkerService.checkBonusTokenSums();
        boolean isSoldTokens = checkerService.checkPurchasedTokenSums();

        return ok("server is running\n" +
                "btc connection: " + btcConnection + "\n" +
                "ltc connection: " + ltcConnection + "\n" +
                "geth connection: " + gethConnection + "\n\n" +
                "bonuses ok: " + isBonusOk + "\n" +
                "bought ok: " + isSoldTokens + "\n");
    }

}
