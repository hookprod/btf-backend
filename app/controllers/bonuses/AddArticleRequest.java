package controllers.bonuses;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Gleb Zykov on 18/01/2018.
 * Request adapter for adding articles
 */
public class AddArticleRequest {

    @NotEmpty
    public String articleUrl;

}
