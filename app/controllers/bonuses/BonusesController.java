package controllers.bonuses;

import com.google.inject.Inject;
import controllers.ApiController;
import controllers.AuthenticatedAction;
import model.Article;
import model.SharingType;
import model.UserProfile;
import model.UserSharingRecord;
import play.mvc.Result;
import play.mvc.With;
import services.BonusService;
import services.UserProfileService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Gleb Zykov on 18/01/2018.
 * Bonus system controllers for admin page api
 */
@With(AuthenticatedAction.class)
public class BonusesController extends ApiController {

    private final UserProfileService userProfileService;
    private final BonusService bonusService;

    @Inject
    public BonusesController(UserProfileService userProfileService,
                             BonusService bonusService) {
        this.bonusService = bonusService;
        this.userProfileService = userProfileService;
    }

    public Result addArticle() {
        AddArticleRequest addArticleRequest = parseBodyAs(AddArticleRequest.class);

        String userId = getUserId();
        Optional<UserProfile> userProfileOptional = userProfileService.findById(userId);
        if(!userProfileOptional.isPresent()) {
            return unauthorizedResponse("user not found");
        }

        Article article = new Article();
        article.setUrl(addArticleRequest.articleUrl);
        article.setUserProfile(userProfileOptional.get());

        article = bonusService.addArticle(article);
        return createdResponse(article);
    }

    public Result getArticles(int page, int size) {
        if(page < 1) {
            throw new IllegalArgumentException("page should be positive");
        }
        if(size > 100 || size < 1) {
            throw new IllegalArgumentException("size parameter should be between 1 and 100");
        }

        addTotalCountHeader(bonusService.getArticlesCount(getUserId()));
        return okResponse(bonusService.getArticles(getUserId(), page, size));
    }

    public Result socialShare() {
        String userId = getUserId();
        Optional<UserProfile> userProfileOptional = userProfileService.findById(userId);
        if(!userProfileOptional.isPresent()) {
            return unauthorizedResponse("user not found");
        }

        SocialBonusRequest socialBonusRequest = parseBodyAs(SocialBonusRequest.class);
        bonusService.addSocialBonuses(userId, socialBonusRequest.eventType);

        return resultResponse("bonuses assigned");
    }

    public Result getSharings() {
        List<SharingType> sharingTypes = bonusService.getAllSharingTypes();
        List<UserSharingRecord> sharingRecords = bonusService.getSharingRecords(getUserId());
        List<SharingRecordAnswer> answerList = sharingTypes.stream()
                .map(sharingType -> {
                    Optional<UserSharingRecord> sharingRecord = sharingRecords.stream()
                            .filter(sharing -> sharing.getSharingType().getId().equals(sharingType.getId()))
                            .findAny();
                    SharingRecordAnswer sharingRecordAnswer = new SharingRecordAnswer();
                    sharingRecordAnswer.setEventType(sharingType.getName());
                    if (sharingRecord.isPresent()) {
                        sharingRecordAnswer.setCompleted(true);
                        sharingRecordAnswer.setTokens(sharingRecord.get().getTokensEarned());
                    } else {
                        sharingRecordAnswer.setCompleted(false);
                        sharingRecordAnswer.setTokens(sharingType.getBonusAmount());
                    }
                    return sharingRecordAnswer;
                })
                .collect(Collectors.toList());
        return okResponse(answerList);
    }


}
