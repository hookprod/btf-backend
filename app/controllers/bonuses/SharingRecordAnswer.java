package controllers.bonuses;

import java.math.BigInteger;

public class SharingRecordAnswer {

    public BigInteger tokens;

    public String eventType;

    public boolean completed;

    public BigInteger getTokens() {
        return tokens;
    }

    public void setTokens(BigInteger tokens) {
        this.tokens = tokens;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
