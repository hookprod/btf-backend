package controllers.bonuses;

import javax.validation.constraints.NotNull;

/**
 * Created by Gleb Zykov on 18/01/2018.
 * request adapter for requesting bonuses for some actions (vk sharing, tweet etc)
 */
public class SocialBonusRequest {

    @NotNull
    public String eventType;

}
