package controllers;

import io.jsonwebtoken.SignatureException;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import services.JwtService;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by Gleb Zykov on 23/01/2018.
 * Provides token authorization
 */
public class AuthenticatedAction extends play.mvc.Action.Simple {

    public static final String SECRET = "MXuri2s0C6JMfl8ejutXPuYmiXXrQrqNdYFFtzQUI7nUo6CUswDnmSjFpbTsnLPt";

    @Override
    public CompletionStage<Result> call(Http.Context ctx) {
        Optional<String> authorization = ctx.request().header("Authorization");
        if(!authorization.isPresent()) {
            return CompletableFuture.completedFuture(
                    unauthorized(Json.toJson(new ErrorResponse("No auth header"))));
        }
        String header = authorization.get();
        if(!header.startsWith("Bearer ")) {
            return CompletableFuture.completedFuture(
                    unauthorized(Json.toJson(new ErrorResponse("Wrong token format"))));
        }
        String token = header.replaceFirst("Bearer ", "");
        try {
            String userId = JwtService.getSubject(token, SECRET);

            if(userId == null || userId.isEmpty()) {
                return CompletableFuture.completedFuture(
                        unauthorized(Json.toJson(new ErrorResponse("no user info"))));
            }
        } catch (SignatureException e) {
            return CompletableFuture.completedFuture(
                    unauthorized(Json.toJson(new ErrorResponse("Wrong token"))));
        }


        return delegate.call(ctx);
    }

    public static String getUserId(Http.Context ctx) {
        String header = ctx.request().header("Authorization").get();
        String token = header.replaceFirst("Bearer ", "");
        return JwtService.getSubject(token, SECRET);
    }
    
}
