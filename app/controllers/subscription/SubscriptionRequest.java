package controllers.subscription;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;

public class SubscriptionRequest {

    @NotNull
    @Email
    public String email;

}
