package controllers.subscription;

import controllers.ApiController;
import play.mvc.Result;
import services.SubscriptionService;

import javax.inject.Inject;

public class SubscriptionController extends ApiController {

    private final SubscriptionService subscriptionService;

    @Inject
    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    public Result subscribeEmail() {
        SubscriptionRequest subscriptionRequest = parseBodyAs(SubscriptionRequest.class);
        subscriptionService.subscribeEmail(subscriptionRequest.email.trim());
        return noContent();
    }

}
