package controllers;

public class ErrorResponse {

    private final String response;

    public ErrorResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

}
