package controllers.admin;

import java.math.BigInteger;

public class IcoStatusResponse {

    public final BigInteger boughtTokens;

    public final BigInteger referralTokens;

    public final BigInteger bonusTokens;

    public IcoStatusResponse(BigInteger boughtTokens, BigInteger referralTokens, BigInteger bonusTokens) {
        this.boughtTokens = boughtTokens;
        this.referralTokens = referralTokens;
        this.bonusTokens = bonusTokens;
    }

}
