package controllers.admin;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;

/**
 * Created by Gleb Zykov on 19/01/2018.
 * request adapter for adding bonuses
 */
public class ManualAddBonusesRequest {

    @NotEmpty
    public String userEmail;

    @Min(1)
    @NotNull
    public BigInteger amount;

    @NotEmpty
    public String comment;

}
