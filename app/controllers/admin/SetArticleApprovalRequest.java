package controllers.admin;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;

/**
 * Created by Gleb Zykov on 18/01/2018.
 */
class SetArticleApprovalRequest {

    @NotNull
    public Long articleId;

    @Min(1)
    public BigInteger reward;

}
