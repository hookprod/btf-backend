package controllers.admin;

import com.google.inject.Inject;
import com.opencsv.CSVWriter;
import controllers.ApiController;
import model.Article;
import model.Subscription;
import model.UserProfile;
import play.mvc.Result;
import services.BonusService;
import services.SubscriptionService;
import services.clearing.ClearingReportService;
import services.IcoService;
import services.UserProfileService;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Gleb Zykov on 11/12/2017.
 * Admin settings controller
 */
public class AdminController extends ApiController {

    private final BonusService bonusService;
    private final UserProfileService userProfileService;
    private final IcoService icoService;
    private final ClearingReportService clearingReportService;
    private final SubscriptionService subscriptionService;

    @Inject
    AdminController(BonusService bonusService,
                    UserProfileService userProfileService,
                    ClearingReportService clearingReportService,
                    SubscriptionService subscriptionService,
                    IcoService icoService) {
        this.bonusService = bonusService;
        this.subscriptionService = subscriptionService;
        this.userProfileService = userProfileService;
        this.icoService = icoService;
        this.clearingReportService = clearingReportService;
    }

    public Result getSubscriptions() throws IOException {
        List<Subscription> allSubscriptions = subscriptionService.getAllSubscriptions();
        String path = "/tmp/subscriptions.csv";
        Writer writer = Files.newBufferedWriter(Paths.get(path));
        final CSVWriter csvWriter;
        csvWriter = new CSVWriter(writer,
                CSVWriter.DEFAULT_SEPARATOR,
                CSVWriter.DEFAULT_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);

        allSubscriptions.forEach(subscription -> {
            csvWriter.writeNext(new String[] {
                    subscription.getEmail()
            });
        });

        csvWriter.flush();
        csvWriter.close();
        return ok(new File(path));

    }

    public Result clearing() throws IOException {
        File file = File.createTempFile("clearingReport_" + System.currentTimeMillis(), ".csv");
        clearingReportService.generateClearingFile(file);
        return ok(file, true);
    }

    public Result getArticles() {
        int page = getPage();
        int size = getSize();

        Set<Article.Status> statuses;

        String statusesStr = request().getQueryString("statuses");
        if(statusesStr != null && !statusesStr.isEmpty()) {
            String[] statusesArray = statusesStr.split(",");
            try {
                statuses = Arrays.stream(statusesArray)
                        .map(Article.Status::valueOf)
                        .collect(Collectors.toSet());
            } catch (IllegalArgumentException ex) {
                return badRequestResponse("count not parse statuses");
            }
        } else {
            statuses = Arrays.stream(Article.Status.values()).collect(Collectors.toSet());
        }
        List<Article> articles = bonusService.getArticles(statuses, page, size);
        addTotalCountHeader(bonusService.getArticlesCount());
        return okResponse(articles);
    }

    public Result approveArticle() {
        SetArticleApprovalRequest setArticleApprovalRequest = parseBodyAs(SetArticleApprovalRequest.class);
        if(setArticleApprovalRequest.reward == null) {
            return badRequestResponse("no reward set");
        }
        bonusService.approveArticle(setArticleApprovalRequest.articleId, setArticleApprovalRequest.reward);
        return noContent();
    }

    public Result declineArticle() {
        SetArticleApprovalRequest setArticleApprovalRequest = parseBodyAs(SetArticleApprovalRequest.class);
        bonusService.declineArticle(setArticleApprovalRequest.articleId);
        return noContent();
    }

    public Result manualAddBonuses() {
        ManualAddBonusesRequest addBonusesRequest = parseBodyAs(ManualAddBonusesRequest.class);
        Optional<UserProfile> profileByLogin = userProfileService.findByEmail(addBonusesRequest.userEmail);
        if(!profileByLogin.isPresent()) {
            throw new IllegalArgumentException("user with login " +
                    addBonusesRequest.userEmail + " is not found");
        }
        icoService.manualAddBonusTokens(profileByLogin.get().getId(), addBonusesRequest.amount,
                addBonusesRequest.comment);
        return resultResponse("bonuses were assigned");
    }

    public Result getIcoStatus() {
        BigInteger bonusTokens = icoService.calculateBonusTokens();
        BigInteger boughtTokens = icoService.calculateBoughtTokens();
        BigInteger referralTokens = icoService.calculateReferralTokens();

        IcoStatusResponse icoStatusResponse = new IcoStatusResponse(boughtTokens, referralTokens, bonusTokens);
        return okResponse(icoStatusResponse);
    }

}
