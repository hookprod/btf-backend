package controllers.stage;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigInteger;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class StageResponse {

    public final BigInteger tokensForEth;

    public final int bonusPercent;

    public final boolean active;

    public final String reason;

    public StageResponse(BigInteger tokensForEth,
                         int bonusPercent,
                         boolean isActive,
                         String reason) {
        this.active = isActive;
        this.reason = reason;
        this.tokensForEth = tokensForEth;
        this.bonusPercent = bonusPercent;
    }
}
