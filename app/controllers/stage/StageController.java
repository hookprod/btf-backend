package controllers.stage;

import com.google.inject.Inject;
import controllers.ApiController;
import model.IcoInfo;
import play.mvc.Result;
import services.IcoService;

import java.math.BigInteger;

public class StageController extends ApiController {

    private final IcoService icoService;

    @Inject
    public StageController(IcoService icoService) {
        this.icoService = icoService;
    }


    public Result getCurrentStageInfo() {
        IcoInfo icoInfo = icoService.getIcoInfo();

        boolean active = true;
        String reason = null;
        BigInteger tokensForEth = BigInteger.ZERO;
        int bonusPercent = 0;

        try {
            icoInfo.assertIcoIsActive();
            bonusPercent = icoInfo.getCurrentStage().getBonusPercent();
            tokensForEth = IcoInfo.BASE_TOKEN_PRICE
                    .multiply(BigInteger.valueOf(100 + bonusPercent))
                    .divide(BigInteger.valueOf(100)).multiply(IcoInfo.TOKEN_MULTIPLIER);
        } catch (IllegalStateException ex) {
            active = false;
            reason = ex.getMessage();
        }

        return okResponse(new StageResponse(tokensForEth, bonusPercent, active, reason));
    }

}
