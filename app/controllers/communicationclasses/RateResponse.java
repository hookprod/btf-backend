package controllers.communicationclasses;

import java.math.BigDecimal;

/**
 * Created by Gleb Zykov on 13/12/2017.
 */
public class RateResponse {

    public final double ethToBtcRate;
    public final double ethToUsdRate;
    public final long rateFixTime;

    public RateResponse(BigDecimal ethToBtcRate, BigDecimal ethToUsdRate, long rateFixTime) {
        this.ethToBtcRate = ethToBtcRate.doubleValue();
        this.ethToUsdRate = ethToUsdRate.doubleValue();
        this.rateFixTime = rateFixTime;
    }
}
