package utils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * utils
 */
public class Formatter {

    public static final String format(LocalDateTime localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return formatter.format(localDateTime);
    }

    public static String formatTimestamp(Timestamp timestamp) {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(timestamp);
    }

    public static String formatToBtcUp(BigDecimal value) {
        return value.setScale(8, BigDecimal.ROUND_UP).stripTrailingZeros().toPlainString();
    }

    public static String formatToBtcDown(BigDecimal value) {
        return value.setScale(8, BigDecimal.ROUND_DOWN).stripTrailingZeros().toPlainString();
    }

    public static String ratePresentation(BigDecimal value) {
        return value.setScale(8, BigDecimal.ROUND_HALF_UP).toString();
    }

    public static String getFullOrderUrl(String orderId) {
        return "http://fscryptoexchange.gtcservices.ru/orders/" + orderId;
    }

    public static String formatEth(BigDecimal value) {
        return value.setScale(18, BigDecimal.ROUND_DOWN)
                .stripTrailingZeros().toPlainString();
    }

    public static String formatWei(BigDecimal value) {
        return value.setScale(18, BigDecimal.ROUND_DOWN).divide(BigDecimal.valueOf(Math.pow(10, 18)), BigDecimal.ROUND_HALF_UP)
                .stripTrailingZeros().toPlainString();
    }

    public static String capitalize(String input) {
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }
}
