import bcJsonRpc.BitcoindClientFactory;
import bcJsonRpc.BitcoindInterface;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.typesafe.config.Config;
import play.Logger;
import services.blockhain.LitecoindInterface;

import java.io.IOException;
import java.net.URL;


/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.
 *
 * Play will automatically use any class called `Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */
public class Module extends AbstractModule {

    @Override
    public void configure() {
    }

    @Provides
    public BitcoindInterface getBitcoinClient(Config config) throws IOException {
        BitcoindClientFactory clientFactory;
        String rpcUrl = config.getString("bitcoind.url");
        String userName = config.getString("bitcoind.username");
        String password = config.getString("bitcoind.password");
        Logger.info("connecting to bitcoind with username " + userName);

        clientFactory = new BitcoindClientFactory(
                new URL(rpcUrl),
                userName,
                password);
        BitcoindInterface client = clientFactory.getClient();
        if (client == null) {
            throw new RuntimeException("could not connect to bitcoind");
        }
        return client;
    }

    @Provides
    public LitecoindInterface getLitecoinClient(Config config) throws IOException {
        BitcoindClientFactory clientFactory;
        String rpcUrl = config.getString("litecoind.url");
        String userName = config.getString("litecoind.username");
        String password = config.getString("litecoind.password");
        Logger.info("connecting to litecoind with username " + userName);

        clientFactory = new BitcoindClientFactory(
                new URL(rpcUrl),
                userName,
                password);
        LitecoindInterface client = clientFactory.getLitecoinClient();
        if (client == null) {
            throw new RuntimeException("could not connect to bitcoind");
        }
        return client;
    }

}
