package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Gleb Zykov on 11/12/2017.
 */
@Entity
public class Settings {

    public static final String BASE_URL = "https://webtest.gtcservices.ru";

    @Id
    @GeneratedValue
    private Long id;

    private Double ethRate = 0d;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getEthRate() {
        return ethRate;
    }

    public void setEthRate(Double ethRate) {
        this.ethRate = ethRate;
    }
}
