package model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Gleb Zykov on 22/01/2018.
 */
@Entity
public class ClearingProcessing {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    private UserProfile userProfile;

    @ElementCollection
    private Set<String> transactions;

    private boolean isProcessed = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public Set<String> getTransactions() {
        if(transactions == null) {
            return new HashSet<>();
        }
        return transactions;
    }

    public void setTransactions(Set<String> transactions) {
        this.transactions = transactions;
    }

    public boolean isProcessed() {
        return isProcessed;
    }

    public void setProcessed(boolean processed) {
        isProcessed = processed;
    }
}
