package model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;

/**
 * Created by Gleb Zykov on 18/01/2018.
 * log of token assignments
 */
@Entity
public class TokenAssignment {

    @Id
    @GeneratedValue
    private Long id;

    public enum Type {
        PURCHASE, MANUAL, SOCIAL_SHARE, ARTICLE
    }

    @CreationTimestamp
    private Timestamp createdAt;

    @Column(unique = true)
    private String transactionId;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Column(precision = 30)
    private BigInteger boughtTokens;

    @Column(precision = 30)
    private BigInteger bonusTokens;

    @Column(precision = 30)
    private BigInteger referralTokens;

    @Column(precision = 30)
    private BigInteger bountyTokens;

    @Column(precision = 30)
    private BigInteger rate;

    @Enumerated(EnumType.STRING)
    private PurchaseTransaction.Type currency;

    @Column(precision = 30)
    private BigInteger amountInCurrency;

    @Column(precision = 30)
    private BigInteger weiAmount;


    @ManyToOne
    private UserProfile inviter;

    @ManyToOne
    private UserProfile beneficiary;

    private String comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigInteger getBoughtTokens() {
        return boughtTokens;
    }

    public void setBoughtTokens(BigInteger boughtTokens) {
        this.boughtTokens = boughtTokens;
    }

    public BigInteger getBonusTokens() {
        return bonusTokens;
    }

    public void setBonusTokens(BigInteger bonusTokens) {
        this.bonusTokens = bonusTokens;
    }

    public BigInteger getReferralTokens() {
        return referralTokens;
    }

    public void setReferralTokens(BigInteger referralTokens) {
        this.referralTokens = referralTokens;
    }

    public UserProfile getInviter() {
        return inviter;
    }

    public void setInviter(UserProfile inviter) {
        this.inviter = inviter;
    }

    public UserProfile getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(UserProfile beneficiary) {
        this.beneficiary = beneficiary;
    }

    public BigInteger getRate() {
        return rate;
    }

    public void setRate(BigInteger rate) {
        this.rate = rate;
    }

    public BigInteger getAmountInCurrency() {
        return amountInCurrency;
    }

    public void setAmountInCurrency(BigInteger amountInCurrency) {
        this.amountInCurrency = amountInCurrency;
    }

    public BigInteger getWeiAmount() {
        return weiAmount;
    }

    public void setWeiAmount(BigInteger weiAmount) {
        this.weiAmount = weiAmount;
    }

    public PurchaseTransaction.Type getCurrency() {
        return currency;
    }

    public void setCurrency(PurchaseTransaction.Type currency) {
        this.currency = currency;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public BigInteger getBountyTokens() {
        return bountyTokens;
    }

    public void setBountyTokens(BigInteger bountyTokens) {
        this.bountyTokens = bountyTokens;
    }
}
