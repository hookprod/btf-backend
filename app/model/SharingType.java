package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigInteger;

@Entity
public class SharingType {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @Column(precision = 30)
    private BigInteger bonusAmount;

    public SharingType() {
    }

    public SharingType(String name, BigInteger bonusAmount) {
        this.name = name;
        this.bonusAmount = bonusAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(BigInteger bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

}
