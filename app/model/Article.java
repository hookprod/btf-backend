package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

/**
 * Created by Gleb Zykov on 18/01/2018.
 */
@Entity
public class Article {

    public enum Status {
        PENDING, APPROVED, DECLINED
    }

    @Id
    @GeneratedValue
    private long id;

    private String url;

    @Enumerated(EnumType.STRING)
    private Status status = Status.PENDING;

    private Timestamp approvedAt;

    @Column(precision = 30)
    private BigInteger tokens;

    @ManyToOne
    private UserProfile userProfile;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @JsonIgnore
    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @JsonIgnore
    public Timestamp getApprovedAt() {
        return approvedAt;
    }

    @JsonProperty("approvedAt")
    public long getUnixTimeApprovedAt() {
        if(approvedAt == null) {
            return 0;
        } else {
            return approvedAt.getTime() / 1000;
        }
    }

    public void setApprovedAt(Timestamp approvedAt) {
        this.approvedAt = approvedAt;
    }

    public BigInteger getTokens() {
        return tokens;
    }

    public void setTokens(BigInteger tokens) {
        this.tokens = tokens;
    }
}
