package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigInteger;

@Entity
public class UserProfile {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(unique = true)
    private String id;

    private String email;
    private String passwordHash;
    private String firstName;
    private String lastName;
    private String referralCode;
    private String phoneNumber;

    private String ethAddress;
    private String btcAddress;
    private String ltcAddress;

    private Long registrationTime;

    @Column(precision = 30)
    private BigInteger tokensBought = BigInteger.ZERO;
    @Column(precision = 30)
    private BigInteger bonusTokens = BigInteger.ZERO;
    @Column(precision = 30)
    private BigInteger referralTokens = BigInteger.ZERO;
    @Column(precision = 30)
    private BigInteger bountyTokens = BigInteger.ZERO;

    @Column(precision = 30)
    private BigInteger ethInvested = BigInteger.ZERO;
    @Column(precision = 30)
    private BigInteger btcInvested = BigInteger.ZERO;
    @Column(precision = 30)
    private BigInteger ltcInvested = BigInteger.ZERO;

    @JsonIgnore
    private String inviterId;

    @JsonIgnore
    private boolean isVerified;

    @JsonIgnore
    private String verificationToken;

    private String erc20WalletAddress;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BigInteger getTokensBought() {
        return tokensBought;
    }

    public void setTokensBought(BigInteger tokensBought) {
        this.tokensBought = tokensBought;
    }

    public BigInteger getBonusTokens() {
        return bonusTokens;
    }

    public void setBonusTokens(BigInteger bonusTokens) {
        this.bonusTokens = bonusTokens;
    }

    public BigInteger getEthInvested() {
        return ethInvested;
    }

    public void setEthInvested(BigInteger ethInvested) {
        this.ethInvested = ethInvested;
    }

    public BigInteger getBtcInvested() {
        return btcInvested;
    }

    public void setBtcInvested(BigInteger btcInvested) {
        this.btcInvested = btcInvested;
    }

    public BigInteger getLtcInvested() {
        return ltcInvested;
    }

    public void setLtcInvested(BigInteger ltcInvested) {
        this.ltcInvested = ltcInvested;
    }

    public String getErc20WalletAddress() {
        return erc20WalletAddress;
    }

    public void setErc20WalletAddress(String erc20WalletAddress) {
        this.erc20WalletAddress = erc20WalletAddress;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getEthAddress() {
        return ethAddress;
    }

    public void setEthAddress(String ethAddress) {
        this.ethAddress = ethAddress;
    }

    public String getBtcAddress() {
        return btcAddress;
    }

    public void setBtcAddress(String btcAddress) {
        this.btcAddress = btcAddress;
    }

    public String getLtcAddress() {
        return ltcAddress;
    }

    public void setLtcAddress(String ltcAddress) {
        this.ltcAddress = ltcAddress;
    }

    @JsonIgnore
    public boolean isVerified() {
        return isVerified;
    }

    @JsonIgnore
    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    @JsonIgnore
    public String getVerificationToken() {
        return verificationToken;
    }

    @JsonIgnore
    public void setVerificationToken(String verificationToken) {
        this.verificationToken = verificationToken;
    }

    @JsonIgnore
    public String getPasswordHash() {
        return passwordHash;
    }

    @JsonIgnore
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @JsonIgnore
    public String getInviterId() {
        return inviterId;
    }

    @JsonIgnore
    public void setInviterId(String inviterId) {
        this.inviterId = inviterId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(Long registrationTime) {
        this.registrationTime = registrationTime;
    }

    public BigInteger getReferralTokens() {
        return referralTokens;
    }

    public void setReferralTokens(BigInteger referralTokens) {
        this.referralTokens = referralTokens;
    }

    public BigInteger getBountyTokens() {
        return bountyTokens;
    }

    public void setBountyTokens(BigInteger bountyTokens) {
        this.bountyTokens = bountyTokens;
    }

}
