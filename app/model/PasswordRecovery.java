package model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Gleb Zykov on 26/01/2018.
 * entity to store info about password recovery requests
 */
@Entity
public class PasswordRecovery {

    @Id
    private String userProfileId;

    private Long lastRecoveryRequestTime;

    @NotEmpty
    private String recoveryToken;

    public String getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(String userProfileId) {
        this.userProfileId = userProfileId;
    }

    public Long getLastRecoveryRequestTime() {
        return lastRecoveryRequestTime;
    }

    public void setLastRecoveryRequestTime(Long lastRecoveryRequestTime) {
        this.lastRecoveryRequestTime = lastRecoveryRequestTime;
    }

    public String getRecoveryToken() {
        return recoveryToken;
    }

    public void setRecoveryToken(String recoveryToken) {
        this.recoveryToken = recoveryToken;
    }
}
