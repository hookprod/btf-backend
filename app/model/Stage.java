package model;

import akka.actor.ProviderSelection;

import java.math.BigInteger;
import java.time.LocalDateTime;

/**
 * Created by Gleb Zykov on 17/01/2018.
 */
public class Stage {

    private int bonusPercent;
    private LocalDateTime startAt;
    private LocalDateTime endAt;
    private BigInteger cap;

    public Stage(int bonusPercent, LocalDateTime startAt, LocalDateTime endAt, BigInteger cap) {
        this.bonusPercent = bonusPercent;
        this.startAt = startAt;
        this.endAt = endAt;
        this.cap = cap;
    }

    public int getBonusPercent() {
        return bonusPercent;
    }

    public LocalDateTime getStartAt() {
        return startAt;
    }

    public LocalDateTime getEndAt() {
        return endAt;
    }

    public BigInteger getCap() {
        return cap;
    }
}
