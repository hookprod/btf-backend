package model;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Created by Gleb Zykov on 17/01/2018.
 * Current ICO state
 */
@Entity
public class IcoInfo {

    public static final BigInteger BASE_TOKEN_PRICE = BigInteger.valueOf(10_000);

    public static final BigInteger TOKEN_MULTIPLIER = BigInteger.valueOf((long) Math.pow(10, 18));
    public static final BigInteger WEI_MULTIPLIER = BigInteger.valueOf((long) Math.pow(10, 18));

    public static final BigInteger SOFTCAP = BigInteger.valueOf(90_000_000).multiply(TOKEN_MULTIPLIER);
    public static final BigInteger HARDCAP = BigInteger.valueOf(150_000_000).multiply(TOKEN_MULTIPLIER);
    public static final BigInteger MINIMAL_TRANSACTION = TOKEN_MULTIPLIER;

    public static final int SOFTCAP_TIMEOUT = 240;

    public static final BigInteger PREICO_CAP = BigInteger.valueOf(20_000_000).multiply(TOKEN_MULTIPLIER);

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(precision = 30)
    private BigInteger soldDuringPreIco = BigInteger.ZERO;

    @Column(precision = 30)
    private BigInteger soldTokens = BigInteger.ZERO;

    @Column(precision = 30)
    private BigInteger bonusTokens = BigInteger.ZERO;

    @Column(precision = 30)
    private BigInteger referralTokens = BigInteger.ZERO;

    @Column(precision = 30)
    private BigInteger bountyTokens = BigInteger.ZERO;

    private LocalDateTime preIcoStartTime;

    private LocalDateTime icoStartTime;

    private LocalDateTime softcapReachedAt;


    @Transient
    private final ArrayList<Stage> stages;

    public IcoInfo() {
        stages = new ArrayList<>();
        stages.add(new Stage(100, LocalDateTime.of(2018, 1, 1, 0, 0), LocalDateTime.of(2018, 2, 1, 0, 0),
                BigInteger.valueOf(20_000_000).multiply(WEI_MULTIPLIER)));

        //first stage of ico
        stages.add(new Stage(30, LocalDateTime.of(2018, 2, 1, 0, 0), LocalDateTime.of(2018, 4, 1, 0, 0),
                BigInteger.valueOf(13_000_000).multiply(WEI_MULTIPLIER)));

        //second stage of ico
        stages.add(new Stage(20, LocalDateTime.of(2018, 2, 1, 0, 0), LocalDateTime.of(2018, 4, 1, 0, 0),
                BigInteger.valueOf(24_000_000).multiply(WEI_MULTIPLIER)));

        stages.add(new Stage(10, LocalDateTime.of(2018, 2, 1, 0, 0), LocalDateTime.of(2018, 4, 1, 0, 0),
                BigInteger.valueOf(33_000_000).multiply(WEI_MULTIPLIER)));

        //todo available tokens
        stages.add(new Stage(0, LocalDateTime.of(2018, 2, 1, 0, 0),
                LocalDateTime.of(2018, 4, 1, 0, 0),
                BigInteger.valueOf(0).multiply(WEI_MULTIPLIER)));
    }

    public void assertIcoIsActive() throws IllegalStateException {
        LocalDateTime now = LocalDateTime.now();

        if(now.isBefore(getPreIcoStartTime())) {
            throw new IllegalStateException("Pre-ICO is not started");
        }

        if(now.isAfter(getIcoEndTime())) {
            throw new IllegalStateException("ICO is ended");
        }

        if(soldTokens.add(bonusTokens).compareTo(SOFTCAP) >= 0) {
            if(now.isAfter(softcapReachedAt.plusHours(SOFTCAP_TIMEOUT))) {
                throw new IllegalStateException("ICO is ended");
            }
        }

        if(soldTokens.add(bonusTokens).compareTo(HARDCAP) >= 0)  {
            throw new IllegalStateException("hardcap is reached");
        }

    }

    /**
     * Get tokens amount with decimals
     * @param tokens tokens without decimals
     * @return tokens amount with decimals
     */
    public static BigInteger tokensAmount(int tokens) {
        return BigInteger.valueOf(tokens).multiply(TOKEN_MULTIPLIER);

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getSoldDuringPreIco() {
        return soldDuringPreIco;
    }

    /**
     *
     * @param soldDuringPreIco with bonuses!
     */
    public void setSoldDuringPreIco(BigInteger soldDuringPreIco) {
        this.soldDuringPreIco = soldDuringPreIco;
    }

    public BigInteger getSoldTokens() {
        return soldTokens;
    }

    public void setSoldTokens(BigInteger soldTokens) {
        this.soldTokens = soldTokens;
    }

    public void addSoldTokens(BigInteger soldTokens) {
        if(this.soldTokens == null) {
            this.soldTokens = BigInteger.ZERO;
        }
        this.soldTokens = this.soldTokens.add(soldTokens);
    }

    public BigInteger getBonusTokens() {
        if(bonusTokens == null) {
            return BigInteger.ZERO;
        }
        return bonusTokens;
    }

    public BigInteger getReferralTokens() {
        return referralTokens;
    }

    public void setReferralTokens(BigInteger referralTokens) {
        this.referralTokens = referralTokens;
    }

    public void addReferralTokens(BigInteger referralTokens) {
        if(this.referralTokens == null) {
            this.referralTokens = BigInteger.ZERO;
        }
        this.referralTokens = this.referralTokens.add(referralTokens);
    }

    public BigInteger getBountyTokens() {
        return bountyTokens;
    }

    public void setBountyTokens(BigInteger bountyTokens) {
        this.bountyTokens = bountyTokens;
    }

    public void addBountyTokens(BigInteger bountyTokens) {
        if(this.bountyTokens == null) {
            this.bountyTokens = BigInteger.ZERO;
        }
        this.bountyTokens = this.bountyTokens.add(bountyTokens);
    }


    public void setBonusTokens(BigInteger bonusTokens) {
        this.bonusTokens = bonusTokens;
    }

    public void addBonusTokens(BigInteger bonusTokens) {
        if(this.bonusTokens == null) {
            this.bonusTokens = BigInteger.ZERO;
        }
        this.bonusTokens = this.bonusTokens.add(bonusTokens);
    }

    public LocalDateTime getSoftcapReachedAt() {
        return softcapReachedAt;
    }

    public void setSoftcapReachedAt(LocalDateTime softcapReachedAt) {
        this.softcapReachedAt = softcapReachedAt;
    }

    public LocalDateTime getPreIcoStartTime() {
        if(preIcoStartTime == null) {
            return LocalDateTime.of(2018, 2, 1, 0, 0);
        }
        return preIcoStartTime;
    }

    public void setPreIcoStartTime(LocalDateTime preIcoStartTime) {
        this.preIcoStartTime = preIcoStartTime;
    }

    public LocalDateTime getIcoStartTime() {
        if(icoStartTime == null) {
            return getPreIcoStartTime().plusDays(30);
        } else {
            return icoStartTime;
        }
    }

    public void setIcoStartTime(LocalDateTime icoStartTime) {
        this.icoStartTime = icoStartTime;
    }

    public LocalDateTime getIcoEndTime() {
        return getIcoStartTime().plusDays(60);
    }

    public Stage getCurrentStage() {
        if(LocalDateTime.now().isBefore(getIcoStartTime())) {
            return stages.get(0);
        } else {
            return getIcoStage(getSoldTokens(), getBonusTokens(), getSoldDuringPreIco());
        }
    }

    public Stage getIcoStage(BigInteger soldTokens, BigInteger bonusTokens, BigInteger soldDuringPreIco) {
        BigInteger soldDuringIco = soldTokens.add(bonusTokens).subtract(soldDuringPreIco);

        BigInteger currentStageCap = BigInteger.ZERO;
        for(int i = 1; i < stages.size(); i++) {
            BigInteger cap = stages.get(i).getCap();
            currentStageCap = currentStageCap.add(cap);
            if(soldDuringIco.compareTo(currentStageCap) < 0) {
                return stages.get(i);
            }
        }
        return stages.get(stages.size() - 1);
    }

}
