package model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
public class RateRecord {

    @Id
    @GeneratedValue
    private Long id;

    @CreationTimestamp
    private Timestamp timestamp;

    @Column(precision = 8, scale = 6)
    private BigDecimal ltcRate;

    @Column(precision = 8, scale = 6)
    private BigDecimal btcRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public BigDecimal getLtcRate() {
        return ltcRate;
    }

    public void setLtcRate(BigDecimal ltcRate) {
        this.ltcRate = ltcRate;
    }

    public BigDecimal getBtcRate() {
        return btcRate;
    }

    public void setBtcRate(BigDecimal btcRate) {
        this.btcRate = btcRate;
    }
}
