package model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
public class UserSharingRecord {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private UserProfile userProfile;

    @ManyToOne
    private SharingType sharingType;

    @Column(precision = 30)
    private BigInteger tokensEarned;

    @JsonIgnore
    public Long getId() {
        return id;
    }

    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public SharingType getSharingType() {
        return sharingType;
    }

    public void setSharingType(SharingType sharingType) {
        this.sharingType = sharingType;
    }

    public BigInteger getTokensEarned() {
        return tokensEarned;
    }

    public void setTokensEarned(BigInteger tokensEarned) {
        this.tokensEarned = tokensEarned;
    }
}
