package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseTransaction {

    public enum Type {BTC, LTC, ETH}

    @Id
    @GeneratedValue
    private Long id;

    private String hash;

    private String userProfileId;

    private boolean success;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Column(precision = 30)
    private BigInteger amount;

    @Column(precision = 30)
    private BigInteger weiAmount;

    @Column(precision = 30)
    private BigInteger rate;

    private Long timestamp;

    private LocalDateTime processedAt;

    private String recipient;

    @Column(precision = 30)
    private BigInteger tokensBought;

    @Column(precision = 30)
    private BigInteger bonusTokens;

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public BigInteger getRate() {
        return rate;
    }

    public void setRate(BigInteger rate) {
        this.rate = rate;
    }

    @JsonIgnore
    public LocalDateTime getProcessedAt() {
        return processedAt;
    }

    public void setProcessedAt(LocalDateTime processedAt) {
        this.processedAt = processedAt;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String to) {
        this.recipient = to;
    }

    public BigInteger getTokensBought() {
        return tokensBought;
    }

    public void setTokensBought(BigInteger tokensBought) {
        this.tokensBought = tokensBought;
    }

    public BigInteger getBonusTokens() {
        return bonusTokens;
    }

    public void setBonusTokens(BigInteger bonusTokens) {
        this.bonusTokens = bonusTokens;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public BigInteger getWeiAmount() {
        return weiAmount;
    }

    public void setWeiAmount(BigInteger weiAmount) {
        this.weiAmount = weiAmount;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @JsonIgnore
    public String getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(String userProfileId) {
        this.userProfileId = userProfileId;
    }

}
