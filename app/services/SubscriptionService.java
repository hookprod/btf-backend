package services;

import model.Subscription;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class SubscriptionService {

    private final JPAApi jpaApi;

    @Inject
    public SubscriptionService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public boolean isSubscribed(String email) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Subscription subscription = em.find(Subscription.class, email.trim());
            return subscription != null;
        });
    }

    public void subscribeEmail(String notFormattedEmail) {
        String email = notFormattedEmail.trim().toLowerCase();
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Subscription subscription = em.find(Subscription.class, email);
            if(subscription != null) {
                throw new IllegalArgumentException("email is already subscribed");
            }
            Subscription newSubscription = new Subscription();
            newSubscription.setEmail(email.trim());
            em.persist(newSubscription);
        });
    }

    public List<Subscription> getAllSubscriptions() {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<Subscription> query = em.createQuery("from Subscription", Subscription.class);
            return query.getResultList();
        });
    }

    public void unsubscribeEmail(String email) {
        String formattedEmail = email.trim().toLowerCase();
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Subscription subscription = em.find(Subscription.class, formattedEmail);
            if(subscription == null) {
                throw new IllegalArgumentException("email is not subscribed");
            }
            Query query = em.createQuery("delete from Subscription where id = :email");
            query.setParameter("email", formattedEmail);
            query.executeUpdate();
        });
    }
}
