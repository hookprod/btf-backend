package services;

import com.google.inject.Inject;
import model.*;
import play.db.jpa.JPAApi;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Gleb Zykov on 17/01/2018.
 * request handling for bonuses
 */
public class BonusService {

    private final JPAApi jpaApi;
    private final IcoService icoService;

    public static final int REFERRAL_BONUS_PERCENT = 5;

    @Inject
    public BonusService(JPAApi jpaApi, IcoService icoService) {
        this.jpaApi = jpaApi;
        this.icoService = icoService;
        initBonusesIfNeeded();
    }

    private void initBonusesIfNeeded() {
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            long count = (long) em.createQuery("select count(*) from SharingType")
                    .getSingleResult();
            if(count == 0) {
                SharingType fbFollow = new SharingType("FACEBOOK_FOLLOW",
                        BigInteger.TEN.multiply(IcoInfo.TOKEN_MULTIPLIER));
                em.persist(fbFollow);

                SharingType twitterFollow = new SharingType("TWITTER_FOLLOW",
                        BigInteger.TEN.multiply(IcoInfo.TOKEN_MULTIPLIER));
                em.persist(twitterFollow);

                SharingType shareFacebook = new SharingType("FACEBOOK_SHARE",
                        BigInteger.TEN.multiply(IcoInfo.TOKEN_MULTIPLIER));
                em.persist(shareFacebook);

                SharingType twitterShare = new SharingType("TWITTER_SHARE",
                        BigInteger.TEN.multiply(IcoInfo.TOKEN_MULTIPLIER));
                em.persist(twitterShare);

                SharingType linkedinShare = new SharingType("LINKEDIN_SHARE",
                        BigInteger.TEN.multiply(IcoInfo.TOKEN_MULTIPLIER));
                em.persist(linkedinShare);
            }
        });
    }

    public Article addArticle(Article article) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();

            Query query = em.createQuery("select count (*) from Article where userProfile.id = :userId and url = :url");
            query.setParameter("userId", article.getUserProfile().getId());
            query.setParameter("url", article.getUrl());
            if((long) query.getSingleResult() > 0) {
                throw new IllegalArgumentException("Article was already added");
            }

            Query alreadyApprovedQuery = em.createQuery("select count (*) from Article where status = :status and url = :url");
            alreadyApprovedQuery.setParameter("url", article.getUrl());
            alreadyApprovedQuery.setParameter("status", Article.Status.APPROVED);
            if((long) alreadyApprovedQuery.getSingleResult() > 0) {
                throw new IllegalArgumentException("Article is already approved");
            }

            em.persist(article);
            return article;
        });
    }

    public List<Article> getArticles(Set<Article.Status> statuses, int page, int count) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<Article> query = em.createQuery("from Article where status in :statuses", Article.class);
            query.setParameter("statuses", statuses);
            query.setFirstResult((page - 1) * count);
            query.setMaxResults(count);
            return query.getResultList();
        });
    }

    public long getArticlesCount() {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Query query = em.createQuery("select count(*) from Article");
            return (long) query.getSingleResult();
        });
    }

    public List<Article> getArticles(String userId, int page, int count) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<Article> query = em.createQuery("from Article where userProfile.id = :userId", Article.class);
            query.setParameter("userId", userId);
            query.setFirstResult((page - 1) * count);
            query.setMaxResults(count);
            return query.getResultList();
        });
    }

    public long getArticlesCount(String userId) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Query query = em.createQuery("select count(*) from Article where userProfile.id = :userId");
            query.setParameter("userId", userId);
            return (long) query.getSingleResult();
        });
    }

    public void approveArticle(Long articleId, BigInteger reward) {
        //todo do in 1 transaction
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Article article = em.find(Article.class, articleId);
            if(article == null) {
                throw new IllegalArgumentException("article not found");
            }
            if(article.getStatus() != Article.Status.PENDING){
                throw new IllegalArgumentException("status has already been set");
            }

            Query alreadyApprovedQuery = em.createQuery("select count (*) from Article where status = :status and url = :url");
            alreadyApprovedQuery.setParameter("url", article.getUrl());
            alreadyApprovedQuery.setParameter("status", Article.Status.APPROVED);
            if((long) alreadyApprovedQuery.getSingleResult() > 0) {
                throw new IllegalArgumentException("Article with this url is already approved");
            }

            article.setStatus(Article.Status.APPROVED);
            article.setApprovedAt(Timestamp.valueOf(LocalDateTime.now()));
            article.setTokens(reward);
            em.merge(article);
            icoService.addArticleBonusTokens(article.getUserProfile().getId(),
                    reward, article.getUrl());
        });
    }

    public void declineArticle(Long articleId) {
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Article article = em.getReference(Article.class, articleId);
            if(article == null) {
                throw new IllegalArgumentException("article not found");
            }
            if(article.getStatus() != Article.Status.PENDING){
                throw new IllegalArgumentException("status has already been set");
            }
            article.setStatus(Article.Status.DECLINED);
            em.merge(article);
        });
    }

    public List<SharingType> getAllSharingTypes() {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<SharingType> sharingTypes = em.createQuery("from SharingType", SharingType.class);
            return sharingTypes.getResultList();
        });
    }

    public List<UserSharingRecord> getSharingRecords(String userId) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<UserSharingRecord> query = em.createQuery("from UserSharingRecord where userProfile.id = :id", UserSharingRecord.class);
            query.setParameter("id", userId);
            return query.getResultList();
        });
    }

    private Optional<SharingType> findSharingTypeByName(String name) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<SharingType> query = em.createQuery("from SharingType where name = :name", SharingType.class);
            query.setParameter("name", name);
            List<SharingType> results = query.getResultList();
            if(results.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(results.get(0));
            }
        });
    }

    public void addSocialBonuses(String userProfileId, String type) {
        Optional<SharingType> sharingTypeByName = findSharingTypeByName(type);

        if(!sharingTypeByName.isPresent()) {
            throw new IllegalArgumentException("no such type");
        }

        SharingType sharingType = sharingTypeByName.get();
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Query query = em.createQuery(
                    "select count(*) from UserSharingRecord where sharingType.id = :id " +
                            "and userProfile.id = :profileId");
            query.setParameter("id", sharingType.getId());
            query.setParameter("profileId", userProfileId);

            long size = (long) query.getSingleResult();
            if(size > 0) {
                throw new IllegalArgumentException("bonuses for this action were already assigned");
            } else {
                UserProfile userProfile = em.find(UserProfile.class, userProfileId);
                if(userProfile == null) {
                    throw new IllegalArgumentException("no profile found");
                }
                UserSharingRecord userSharingRecord = new UserSharingRecord();
                userSharingRecord.setUserProfile(userProfile);
                userSharingRecord.setSharingType(sharingType);
                userSharingRecord.setTokensEarned(sharingType.getBonusAmount());
                em.persist(userSharingRecord);
            }
        });
        icoService.addSocialBonusTokens(userProfileId, sharingType.getName(), sharingType.getBonusAmount());
    }

}
