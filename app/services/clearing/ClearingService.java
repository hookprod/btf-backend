package services.clearing;

import com.google.inject.Inject;
import model.ClearingProcessing;
import model.UserProfile;
import play.Logger;
import play.db.jpa.JPAApi;
import services.UserProfileService;
import services.blockhain.ethereum.EthereumBlockchainService;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Collectors;

/**
 * Created by Gleb Zykov on 22/01/2018.
 * Clearing - send tokens to users according to their balances
 */
public class ClearingService {

    private final JPAApi jpaApi;
    private final UserProfileService userProfileService;
    private final EthereumBlockchainService ethereumBlockchainService;

    @Inject
    public ClearingService(JPAApi jpaApi, UserProfileService userProfileService,
                           EthereumBlockchainService ethereumBlockchainService) {
        this.jpaApi = jpaApi;
        this.userProfileService = userProfileService;
        this.ethereumBlockchainService = ethereumBlockchainService;
    }

    public void runClearing() {
        List<UserProfile> profiles = userProfileService.getAllProfiles()
                .stream()
                .filter(userProfile -> userProfile.getErc20WalletAddress() != null &&
                        !userProfile.getErc20WalletAddress().isEmpty())
                .collect(Collectors.toList());

        boolean allProcessed = false;

        while (!allProcessed) {
            Logger.info("---------------------------running clearing stage-------------------------------");
            allProcessed = runClearingStage(profiles);
        }
    }

    /**
     *
     * @return true if all profiles were processed
     */
    private boolean runClearingStage(List<UserProfile> profiles) {
        boolean allProcessed = true;
        for(UserProfile profile : profiles) {
            try {
                Logger.info("processing profile " + profile.getEmail());
                boolean processed = processProfile(profile);
                if(!processed) {
                    allProcessed = false;
                }
            } catch (Exception ex) {
                Logger.error("error on processing transaction", ex);
                allProcessed = false;
            }
        }
        return allProcessed;
    }

    /**
     * Run processing for user
     * @param userProfile user profile
     * @return true if tokens were successfully sent to user profile address
     */
    private boolean processProfile(UserProfile userProfile) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<ClearingProcessing> query = em.createQuery("from ClearingProcessing where userProfile.id = :id",
                    ClearingProcessing.class);
            query.setParameter("id", userProfile.getId());
            List<ClearingProcessing> resultList = query.getResultList();
            ClearingProcessing clearingProcessing;
            if(resultList.isEmpty()) {
                clearingProcessing = new ClearingProcessing();
                clearingProcessing.setUserProfile(userProfile);
                em.persist(clearingProcessing);
            } else  {
                clearingProcessing = resultList.get(0);
            }

            boolean processed;

            if(!checkTransactionProcessed(em, clearingProcessing)) {
                Logger.info("sending tokens to " + userProfile.getEmail());

                BigInteger amount = userProfile.getBonusTokens().add(userProfile.getTokensBought());
                String transactionHash = ethereumBlockchainService.sendTokens(userProfile.getErc20WalletAddress(),
                        amount);
                clearingProcessing.getTransactions().add(transactionHash);
                em.merge(clearingProcessing);
                processed = false;
            } else {
                processed = true;
            }

            return processed;

        });
    }

    /**
     * Check if transaction is processes and write to db its status if processed
     * @param em entity manager to use for updating db
     * @param clearingProcessing user clearing processing info
     * @return true if transaction is processes
     */
    private boolean checkTransactionProcessed(EntityManager em, ClearingProcessing clearingProcessing) {
        if(clearingProcessing.isProcessed()) {
            return true;
        } else {
            //check all transactions
            Set<String> transactions = clearingProcessing.getTransactions();
            CopyOnWriteArraySet<String> set = new CopyOnWriteArraySet<>();
            set.addAll(transactions);
            set.forEach(transaction -> {
                if(ethereumBlockchainService.isTransactionSuccessfulAndConfirmed(transaction)) {
                    clearingProcessing.setProcessed(true);
                    em.merge(clearingProcessing);
                } else {
                    Logger.info("transaction with id " + transaction + " is failed");
                }
            });
        }
        return false;
    }


}
