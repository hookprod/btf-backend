package services.clearing;

import com.opencsv.CSVWriter;
import model.TokenAssignment;
import model.UserProfile;
import play.db.jpa.JPAApi;
import services.BonusService;
import services.UserProfileService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ClearingReportService {

    private final JPAApi jpaApi;

    private final UserProfileService userProfileService;

    private final BonusService bonusService;

    @Inject
    public ClearingReportService(JPAApi jpaApi,
                                 UserProfileService userProfileService,
                                 BonusService bonusService) throws IOException {
        this.jpaApi = jpaApi;
        this.bonusService = bonusService;
        this.userProfileService = userProfileService;
    }

    public void generateClearingFile(File file) throws IOException {
        List<UserProfile> allProfiles = userProfileService.getAllProfiles();

        Writer writer = Files.newBufferedWriter(file.toPath());
        final CSVWriter csvWriter;
        csvWriter = new CSVWriter(writer,
                CSVWriter.DEFAULT_SEPARATOR,
                CSVWriter.DEFAULT_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);

        csvWriter.writeNext(new String[] {
                "Email",
                "ERC20 address",
                "BTC invested",
                "BTC address",
                "ETH invested",
                "ETH address",
                "LTC invested",
                "LTC address",
                "Количество токенов",
                "Количество бонусных токенов",
                "Баунти программа, действия (шаринги), отдельными ячейками",
                "Количество токенов за баунти",
                "Количество рефералов",
                "Количество токенов за рефералов",
                "Количество статей",
                "Количество токенов за статьи",
                "Количество вручную начисленных токенов",
                "Суммарное количество токенов для вывода",
        });

        allProfiles.forEach(userProfile -> writeProfile(csvWriter, userProfile));

        csvWriter.flush();
        csvWriter.close();
    }

    private void writeProfile(CSVWriter csvWriter, UserProfile userProfile) {
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();

            List<TokenAssignment> assignments = getSocialTokenAssignments(userProfile, em);

            String sharings = getSharingItem(assignments);

            BigInteger sharingBonuses = BigInteger.ZERO;
            for(TokenAssignment assignment: assignments) {
                sharingBonuses = sharingBonuses.add(assignment.getBountyTokens());
            }

            BigInteger referralTokens = getReferralTokens(em, userProfile);
            BigInteger articleBonusesCount = getArticleBonusesCount(em, userProfile);
            BigInteger manualBonusesCount = getManualBonusesCount(em, userProfile);
            csvWriter.writeNext(new String[] {
                    userProfile.getEmail(),
                    userProfile.getErc20WalletAddress(),
                    userProfile.getBtcInvested().toString(),
                    userProfile.getBtcAddress(),
                    userProfile.getEthInvested().toString(),
                    userProfile.getEthAddress(),
                    userProfile.getLtcInvested().toString(),
                    userProfile.getLtcAddress(),
                    userProfile.getTokensBought().toString(),
                    userProfile.getBonusTokens().toString(),
                    sharings,
                    sharingBonuses.toString(),
                    String.valueOf(getReferralsCount(em, userProfile)),
                    referralTokens.toString(),
                    String.valueOf(bonusService.getArticlesCount(userProfile.getId())),
                    articleBonusesCount.toString(),
                    manualBonusesCount.toString(),
                    userProfile.getBonusTokens().add(userProfile.getTokensBought()).toString()
            });

            BigInteger overallTokensFromUserProfile = userProfile.getBonusTokens().add(userProfile.getTokensBought());
            BigInteger calculatedTokens = getPurchasedTokens(em, userProfile)
                    .add(getBonusPurchasedTokens(em, userProfile))
                    .add(articleBonusesCount)
                    .add(referralTokens)
                    .add(manualBonusesCount);

            System.out.println("IS OK: " + overallTokensFromUserProfile.equals(calculatedTokens));
        });
    }

    public BigInteger getPurchasedTokens(EntityManager em, UserProfile userProfile) {
        Query query = em.createQuery("select sum (boughtTokens) from TokenAssignment p " +
                "where p.beneficiary.id = :profileId and type = :type");
        query.setParameter("profileId", userProfile.getId());
        query.setParameter("type", TokenAssignment.Type.PURCHASE);
        return returnValueOrZero((BigInteger) query.getSingleResult());
    }

    public BigInteger getBonusPurchasedTokens(EntityManager em, UserProfile userProfile) {
        Query query = em.createQuery("select sum (bonusTokens) from TokenAssignment p " +
                "where p.beneficiary.id = :profileId");
        query.setParameter("profileId", userProfile.getId());
        return returnValueOrZero((BigInteger) query.getSingleResult());
    }

    public BigInteger getManualBonusesCount(EntityManager em, UserProfile userProfile) {
        Query boughtTokensQuery = em.createQuery("select sum (boughtTokens) from TokenAssignment p " +
                "where p.beneficiary.id = :profileId " +
                "and type = :bonusesType");
        boughtTokensQuery.setParameter("profileId", userProfile.getId());
        boughtTokensQuery.setParameter("bonusesType", TokenAssignment.Type.MANUAL);
        BigInteger boughtTokens = (BigInteger) boughtTokensQuery.getSingleResult();

        Query bonusTokensQuery = em.createQuery("select sum (bonusTokens) from TokenAssignment p " +
                "where p.beneficiary.id = :profileId " +
                "and type = :bonusesType");
        bonusTokensQuery.setParameter("profileId", userProfile.getId());
        bonusTokensQuery.setParameter("bonusesType", TokenAssignment.Type.MANUAL);

        BigInteger bonusTokens = (BigInteger) bonusTokensQuery.getSingleResult();
        return returnValueOrZero(bonusTokens).add(returnValueOrZero(boughtTokens));
    }

    public BigInteger getArticleBonusesCount(EntityManager em, UserProfile userProfile) {
        Query query = em.createQuery("select sum (bountyTokens) from TokenAssignment p " +
                "where p.beneficiary.id = :profileId " +
                "and type = :bonusesType");
        query.setParameter("profileId", userProfile.getId());
        query.setParameter("bonusesType", TokenAssignment.Type.ARTICLE);
        return returnValueOrZero((BigInteger) query.getSingleResult());
    }

    public long getReferralsCount(EntityManager em, UserProfile userProfile) {
        Query query = em.createQuery("select count (*) from UserProfile p " +
                "where p.inviterId = :inviterId and p.isVerified = true");
        query.setParameter("inviterId", userProfile.getId());
        return (long) query.getSingleResult();
    }

    public BigInteger getReferralTokens(EntityManager em, UserProfile userProfile) {
        Query query = em.createQuery("select sum (referralTokens) from TokenAssignment p " +
                "where p.inviter.id = :inviterId");
        query.setParameter("inviterId", userProfile.getId());
        return returnValueOrZero((BigInteger) query.getSingleResult());
    }

    private BigInteger returnValueOrZero(BigInteger result) {
        if(result == null) {
            return BigInteger.ZERO;
        } else {
            return result;
        }
    }

    private String getSharingItem(List<TokenAssignment> assignments) {
        StringBuilder builder = new StringBuilder();
        assignments.forEach(assignment -> {
            builder.append(assignment.getComment())
                    .append(",")
                    .append(assignment.getBonusTokens())
                    .append(";");

        });
        return builder.toString();
    }

    private List<TokenAssignment> getSocialTokenAssignments(UserProfile userProfile, EntityManager em) {
        TypedQuery<TokenAssignment> query = em.createQuery(
                "from TokenAssignment where beneficiary.id = :userprofileid and type = :socialType",
                TokenAssignment.class);
        query.setParameter("userprofileid", userProfile.getId());
        query.setParameter("socialType", TokenAssignment.Type.SOCIAL_SHARE);
        return query.getResultList();
    }


}
