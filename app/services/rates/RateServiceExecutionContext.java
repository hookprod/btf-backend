package services.rates;

import akka.actor.ActorSystem;
import com.google.inject.Inject;
import play.libs.concurrent.CustomExecutionContext;

public class RateServiceExecutionContext extends CustomExecutionContext {

    @Inject
    public RateServiceExecutionContext(ActorSystem actorSystem) {
        super(actorSystem, "rate-dispatcher");
    }
}
