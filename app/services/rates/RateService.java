package services.rates;

import akka.actor.ActorSystem;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.kevinsawicki.http.HttpRequest;
import model.RateRecord;
import org.springframework.stereotype.Service;
import play.Logger;
import play.db.jpa.JPAApi;
import play.libs.Json;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Service for getting current exchange rate
 */
@Service
public class RateService {

    private final JPAApi jpaApi;

    @Inject
    public RateService(JPAApi jpaApi, ActorSystem actorSystem, RateServiceExecutionContext rateServiceExecutionContext) {
        this.jpaApi = jpaApi;
        actorSystem.scheduler().schedule(
                Duration.create(30, TimeUnit.SECONDS), // initialDelay
                Duration.create(1, TimeUnit.MINUTES), // interval
                this::saveRatesToDb,
                rateServiceExecutionContext // using the custom executor
        );
    }

    public RateRecord getRatesForTime(Timestamp timestamp) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<RateRecord> query = em.createQuery("from RateRecord where timestamp < :threshold order by timestamp desc",
                    RateRecord.class);
            query.setParameter("threshold", timestamp);
            query.setMaxResults(1);
            List<RateRecord> results = query.getResultList();
            if(results.isEmpty()) {
                throw new IllegalStateException("no rates yet");
            }
            return results.get(0);
        });
    }

    private void saveRatesToDb() {
        System.out.println("saving rates to database");
        try {
            RateRecord rateRecord = new RateRecord();
            rateRecord.setBtcRate(getBtcToEthRate());
            rateRecord.setLtcRate(getLtcToEthRate());
            jpaApi.withTransaction(() -> {
                EntityManager em = jpaApi.em();
                em.persist(rateRecord);
            });
        } catch (Exception ex) {
            Logger.error("error on getting rates", ex);
        }
    }

    private BigDecimal getBtcToEthRate() throws IOException {
        String ethTickerStr = HttpRequest.get("https://api.coinmarketcap.com/v1/ticker/ethereum").body();
        CoinMarketCapResponse.List ethTicker = Json.fromJson(Json.parse(ethTickerStr), CoinMarketCapResponse.List.class);
        return BigDecimal.ONE.setScale(6, BigDecimal.ROUND_HALF_UP)
                .divide(ethTicker.get(0).priceBtc, BigDecimal.ROUND_HALF_UP);
    }

    private BigDecimal getLtcToEthRate() throws IOException {
        String ethTickerStr = HttpRequest.get("https://api.coinmarketcap.com/v1/ticker/ethereum").body();
        CoinMarketCapResponse.List ethTicker = Json.fromJson(Json.parse(ethTickerStr), CoinMarketCapResponse.List.class);

        String ltcTickerStr = HttpRequest.get("https://api.coinmarketcap.com/v1/ticker/litecoin").body();
        CoinMarketCapResponse.List ltcTicker = Json.fromJson(Json.parse(ltcTickerStr), CoinMarketCapResponse.List.class);

        return ltcTicker.get(0).priceBtc.divide(ethTicker.get(0).priceBtc, BigDecimal.ROUND_HALF_UP);
    }

//    public BigDecimal getPoloniexBtcToEtherRate() throws IOException{
//        String response = HttpRequest.get("https://poloniex.com/public?command=returnTicker").body();
//        PoloniexResponse poloniexResponse = Json.fromJson(Json.parse(response), PoloniexResponse.class);
//        return poloniexResponse.btcToEth.highestBid;
//    }
//
//    public PoloniexResponse getPoloniexRates() throws IOException {
//        String response = HttpRequest.get("https://poloniex.com/public?command=returnTicker").body();
//        PoloniexResponse poloniexResponse = Json.fromJson(Json.parse(response), PoloniexResponse.class);
//        return poloniexResponse;
//    }

    private static class CoinMarketCapResponse {
        @JsonProperty("price_usd")
        BigDecimal priceUsd;
        @JsonProperty("price_btc")
        BigDecimal priceBtc;
        public static class List extends ArrayList<CoinMarketCapResponse> {}
    }

    public static class PoloniexResponse {
        @JsonProperty("BTC_ETH")
        public TickerInfo btcToEth;

        @JsonProperty("USDT_ETH")
        public TickerInfo ethToUsd;
    }

    public static class TickerInfo {
        public String id;
        public BigDecimal last;
        public BigDecimal lowestAsk;
        public BigDecimal highestBid;
    }

}
