package services;

import com.google.inject.Inject;
import org.springframework.stereotype.Service;
import play.Logger;

/**
 * Created by Gleb Zykov on 12/12/2017.
 * user notification service
 */
@Service
public class UserNotificationService {

    private final MailService mailService;

    @Inject
    public UserNotificationService(MailService mailService) {
        this.mailService = mailService;
    }

    public void sendConfirmationEmail(String email, String confirmationUrl) {
        Logger.info("sending confirmation url to " + email);
        mailService.sendMail(email, "BITRUST email confirmation",
                "Please open this url to confirm your email: " + confirmationUrl);
    }

    public void sendPasswordRecoveryEmail(String email, String recoveryUrl) {
        mailService.sendMail(email, "BITRUST password recovery",
                "Please use this link to recover your password: " + recoveryUrl);
    }

}
