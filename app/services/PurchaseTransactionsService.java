package services;

import model.PurchaseTransaction;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class PurchaseTransactionsService {

    private final JPAApi jpaApi;

    @Inject
    public PurchaseTransactionsService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            em.persist(new PurchaseTransaction());
        });
    }

    public boolean isTransactionProcessed(PurchaseTransaction.Type type, String hash) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Query query = em.createQuery("select count (*) from PurchaseTransaction where hash = :hash and type = :type");
            query.setParameter("hash", hash);
            query.setParameter("type", type);
            return (long) query.getSingleResult() > 0;
        });
    }

    public void save(PurchaseTransaction transaction) {
        System.out.println("saving transaction " + transaction.getHash());
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            em.persist(transaction);
        });
    }

    public List<PurchaseTransaction> getTransactions(String userProfileId, int page, int size) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<PurchaseTransaction> query =
                    em.createQuery("from PurchaseTransaction where userProfileId = :profileId",
                        PurchaseTransaction.class);
            query.setFirstResult((page - 1) * size);
            query.setMaxResults(size);
            query.setParameter("profileId", userProfileId);
            return query.getResultList();
        });
    }

    public long getTransactionsCount(String userProfileId) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Query query =
                    em.createQuery("select count(*) from PurchaseTransaction " +
                                    "where userProfileId = :profileId");
            query.setParameter("profileId", userProfileId);
            return (long) query.getSingleResult();
        });
    }

}
