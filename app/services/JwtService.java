package services;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

/**
 * Created by Gleb Zykov on 23/01/2018.
 * Service for generating and checking JWT tokens
 */
public class JwtService {

    public static String generateJwt(String signKey, String subject) {
        return Jwts.builder()
                .setSubject(subject)
                .signWith(SignatureAlgorithm.HS512, signKey)
                .compact();
    }

    public static String getSubject(String value, String key) throws SignatureException {
        return Jwts.parser().setSigningKey(key).parseClaimsJws(value).getBody().getSubject();
    }

}
