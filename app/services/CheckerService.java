package services;

import model.IcoInfo;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigInteger;

/**
 * service to check if sums
 */
public class CheckerService {

    private final JPAApi jpaApi;

    @Inject
    public CheckerService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public boolean checkBonusTokenSums() {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            BigInteger bonusTokens = (BigInteger) em.createQuery(
                    "select sum(p.bonusTokens) from UserProfile p")
                    .getSingleResult();
            IcoInfo icoInfo = em.find(IcoInfo.class, 1L);
            return icoInfo.getBonusTokens().equals(bonusTokens);
        });
    }

    public boolean checkPurchasedTokenSums() {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            BigInteger bonusTokens = (BigInteger) em.createQuery(
                    "select sum(p.tokensBought) from UserProfile p")
                    .getSingleResult();
            IcoInfo icoInfo = em.find(IcoInfo.class, 1L);
            return icoInfo.getSoldTokens().equals(bonusTokens);
        });
    }

}
