package services;

import model.*;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Optional;

import static model.IcoInfo.MINIMAL_TRANSACTION;
import static model.IcoInfo.TOKEN_MULTIPLIER;

@Singleton
public class IcoService {

    private final JPAApi jpaApi;

    @Inject
    public IcoService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
        createIcoInfoIfNeeded();
    }

    public synchronized void makePurchase(UserProfile profile, PurchaseTransaction transaction) {
        if(!transaction.isSuccess()) {
            throw new IllegalArgumentException("transaction is not successful");
        }

        //calculate bought tokens

        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();

            String comment = "";

            BigInteger tokensBought = calculateBoughtTokens(transaction);

            if(tokensBought.compareTo(MINIMAL_TRANSACTION) < 0) {
                tokensBought = BigInteger.ZERO;
                comment = "amount is less than minimal transaction";
            }

            IcoInfo icoInfo = getIcoInfo();
            try {
                icoInfo.assertIcoIsActive();
            } catch (IllegalStateException ex) {
                //cannot make purchase
                tokensBought = BigInteger.ZERO;
                comment = ex.getMessage();
            }

            Stage currentStage = icoInfo.getCurrentStage();

            //calculate bonus
            int bonusPercent = currentStage.getBonusPercent();
            BigInteger bonusTokens = calculateBonuses(tokensBought, bonusPercent);

            //assign bought tokens

            updateProfile(profile, tokensBought, bonusTokens, transaction);

            TokenAssignment tokenAssignment = new TokenAssignment();
            tokenAssignment.setTransactionId(transaction.getHash());
            tokenAssignment.setType(TokenAssignment.Type.PURCHASE);
            tokenAssignment.setBoughtTokens(tokensBought);
            tokenAssignment.setBeneficiary(profile);
            tokenAssignment.setBonusTokens(bonusTokens);

            tokenAssignment.setComment(comment);

            tokenAssignment.setCurrency(transaction.getType());
            tokenAssignment.setAmountInCurrency(transaction.getAmount());
            tokenAssignment.setWeiAmount(transaction.getWeiAmount());
            tokenAssignment.setRate(transaction.getRate());

            //assign referral tokens to inviter
            BigInteger referralTokens = BigInteger.ZERO;

            if(profile.getInviterId() != null && !profile.getInviterId().isEmpty()) {
                //set referral bonus
                referralTokens = calculateBonuses(tokensBought, BonusService.REFERRAL_BONUS_PERCENT);

                Optional<UserProfile> referralProfileOpt = Optional.ofNullable(
                        em.find(UserProfile.class, profile.getInviterId()));

                if(referralProfileOpt.isPresent()) {
                    tokenAssignment.setReferralTokens(referralTokens);

                    UserProfile inviter = referralProfileOpt.get();
                    tokenAssignment.setInviter(inviter);

                    inviter.setReferralTokens(inviter.getReferralTokens().add(referralTokens));
                    em.persist(inviter);
                }
            }

            em.persist(tokenAssignment);

            updateIcoInfo(icoInfo, tokensBought, bonusTokens, referralTokens);

            em.merge(icoInfo);
            em.merge(profile);
        });

    }

    public void updateIcoInfo(IcoInfo icoInfo, BigInteger tokensBought, BigInteger bonusTokens, BigInteger referralTokens) {
        //set sold during pre ico tokens if needed
        if(icoInfo.getCurrentStage().getBonusPercent() != 100 && icoInfo.getSoldDuringPreIco() == null) {
            icoInfo.setSoldDuringPreIco(icoInfo.getSoldTokens().add(icoInfo.getBonusTokens()));
        }

        icoInfo.addBonusTokens(bonusTokens);
        icoInfo.addReferralTokens(referralTokens);
        icoInfo.addSoldTokens(tokensBought);

        if(icoInfo.getCurrentStage().getBonusPercent() == 100) { //if we are on pre ico stage
            //set ico start time (ico stage) if we reached pre ico cap
            if(icoInfo.getSoldTokens().add(icoInfo.getBonusTokens()).compareTo(IcoInfo.PREICO_CAP) >= 0) {
                icoInfo.setIcoStartTime(LocalDateTime.now());
                icoInfo.setSoldDuringPreIco(icoInfo.getSoldTokens().add(icoInfo.getBonusTokens()));
            }
        } else {
            //if we are on ico stage and reached softcap
            if(icoInfo.getSoldTokens().add(icoInfo.getBonusTokens()).compareTo(IcoInfo.SOFTCAP) >=0 &&
                    icoInfo.getSoftcapReachedAt() == null) {
                icoInfo.setSoftcapReachedAt(LocalDateTime.now());
            }
        }

    }

    BigInteger calculateBoughtTokens(PurchaseTransaction transaction) {
        BigInteger rate = IcoInfo.BASE_TOKEN_PRICE.multiply(IcoInfo.TOKEN_MULTIPLIER);
        return rate.multiply(transaction.getWeiAmount()).divide(TOKEN_MULTIPLIER);
    }

    private void updateProfile(UserProfile profile, BigInteger tokensBought,
                               BigInteger bonusTokens, PurchaseTransaction transaction) {
        profile.setTokensBought(profile.getTokensBought().add(tokensBought));

        //assign bonus tokens
        profile.setBonusTokens(profile.getBonusTokens().add(bonusTokens));

        switch (transaction.getType()) {
            case ETH:
                profile.setEthInvested(profile.getEthInvested().add(transaction.getAmount()));
                break;
            case BTC:
                profile.setBtcInvested(profile.getBtcInvested().add(transaction.getAmount()));
                break;
            case LTC:
                profile.setLtcInvested(profile.getLtcInvested().add(transaction.getAmount()));
                break;
        }
    }

    BigInteger calculateBonuses(BigInteger tokensBought, int bonusPercent) {
        return tokensBought.multiply(BigInteger.valueOf(bonusPercent)).divide(BigInteger.valueOf(100));
    }

    public IcoInfo getIcoInfo() {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            return em.find(IcoInfo.class, 1L);
        });
    }

    private void createIcoInfoIfNeeded() {
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            IcoInfo icoInfo = em.find(IcoInfo.class, 1L);
            if (icoInfo == null) {
                icoInfo = new IcoInfo();
                em.persist(icoInfo);
            }
            return icoInfo;
        });
    }

    public void manualAddBonusTokens(String userProfileId, BigInteger tokensAmount,
                                      String comment) {
        IcoInfo icoInfo = getIcoInfo();
        int bonusPercent = icoInfo.getCurrentStage().getBonusPercent();
        BigInteger bonuses = calculateBonuses(tokensAmount, bonusPercent);
        
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();

            updateIcoInfo(icoInfo, tokensAmount, bonuses, BigInteger.ZERO);
            em.merge(icoInfo);

            TokenAssignment tokenAssignment = new TokenAssignment();
            UserProfile userProfile = em.find(UserProfile.class, userProfileId);
            if(userProfile == null) {
                throw new IllegalArgumentException("no user profile with provided id found");
            }
            tokenAssignment.setBeneficiary(userProfile);
            tokenAssignment.setBoughtTokens(tokensAmount);
            tokenAssignment.setBonusTokens(bonuses);
            tokenAssignment.setType(TokenAssignment.Type.MANUAL);
            tokenAssignment.setComment(comment);
            em.persist(tokenAssignment);

            userProfile.setBonusTokens(userProfile.getBonusTokens().add(bonuses));
            userProfile.setTokensBought(userProfile.getTokensBought().add(tokensAmount));
            em.merge(userProfile);
        });

    }

    public void addArticleBonusTokens(String userProfileId, BigInteger bonusTokens, String articleUrl) {
        addBountyTokens(TokenAssignment.Type.ARTICLE, userProfileId, bonusTokens, articleUrl);
    }

    public void addSocialBonusTokens(String userProfileId, String type,
                                      BigInteger bountyTokens) {
        //check if this assignment already exists
        boolean alreadyAssigned = jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();

            //in comment is share type
            Query query = em.createQuery("select count (*) from TokenAssignment where beneficiary.id = :userId and " +
                    "type =  :socialType and comment = :comment");
            query.setParameter("socialType", TokenAssignment.Type.SOCIAL_SHARE);
            query.setParameter("userId", userProfileId);
            query.setParameter("comment", type);
            return (long) query.getSingleResult() > 0;
        });

        if(alreadyAssigned) {
            throw new IllegalArgumentException("tokens for this event were already assigned");
        }

        addBountyTokens(TokenAssignment.Type.SOCIAL_SHARE, userProfileId, bountyTokens, type);
    }

    private synchronized void addBountyTokens(TokenAssignment.Type type, String userProfileId,
                                                  BigInteger bountyTokens, String comment) {
        IcoInfo icoInfo = getIcoInfo();
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();

            UserProfile userProfile = em.find(UserProfile.class, userProfileId);
            if(userProfile == null) {
                throw new IllegalArgumentException("User not found");
            }

            userProfile.setBountyTokens(userProfile.getBountyTokens().add(bountyTokens));
            em.merge(userProfile);

            TokenAssignment tokenAssignment = new TokenAssignment();
            tokenAssignment.setBeneficiary(userProfile);
            tokenAssignment.setBountyTokens(bountyTokens);
            tokenAssignment.setType(type);
            tokenAssignment.setComment(comment);
            em.persist(tokenAssignment);

            icoInfo.setBountyTokens(icoInfo.getBountyTokens().add(bountyTokens));
            em.merge(icoInfo);
        });
    }

    public BigInteger calculateReferralTokens() {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Query query = em.createQuery("select sum(referralTokens) from TokenAssignment");
            return (BigInteger) query.getSingleResult();
        });
    }

    public BigInteger calculateBonusTokens() {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Query query = em.createQuery("select sum(bonusTokens) from TokenAssignment");
            return (BigInteger) query.getSingleResult();
        });
    }

    public BigInteger calculateBoughtTokens() {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Query query = em.createQuery("select sum(boughtTokens) from TokenAssignment");
            return (BigInteger) query.getSingleResult();
        });
    }

}
