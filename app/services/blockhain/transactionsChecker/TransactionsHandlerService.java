package services.blockhain.transactionsChecker;

import bcJsonRpc.pojo.Transaction;
import model.IcoInfo;
import model.PurchaseTransaction;
import model.RateRecord;
import model.UserProfile;
import play.Logger;
import services.PurchaseTransactionsService;
import services.blockhain.BitcoinBlockchainService;
import services.blockhain.BtcLikeBlockchainService;
import services.blockhain.ethereum.EthereumBlockchainService;
import services.blockhain.LitecoinBlockchainService;
import services.rates.RateService;
import services.IcoService;
import services.UserProfileService;
import services.blockhain.ethereum.EthTransaction;

import javax.inject.Inject;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class TransactionsHandlerService {

    public static final int ETHEREUM_TRANSACTION_CONFIRMATIONS = 0;
    public static final int BTC_LIKE_BLOCKCHAIN_TRANSACTION_CONFIRMATIONS = 0;
    
    private final BitcoinBlockchainService bitcoinBlockchainService;
    private final LitecoinBlockchainService litecoinBlockchainService;
    private final EthereumBlockchainService ethereumBlockchainService;
    private final PurchaseTransactionsService purchaseTransactionsService;
    private final UserProfileService userProfileService;
    private final IcoService icoService;
    private final RateService rateService;

    @Inject
    public TransactionsHandlerService(BitcoinBlockchainService bitcoinBlockchainService,
                                      LitecoinBlockchainService litecoinBlockchainService,
                                      EthereumBlockchainService ethereumBlockchainService,
                                      PurchaseTransactionsService purchaseTransactionsService,
                                      IcoService icoService,
                                      UserProfileService userProfileService,
                                      RateService rateService) {
        this.bitcoinBlockchainService = bitcoinBlockchainService;
        this.litecoinBlockchainService = litecoinBlockchainService;
        this.ethereumBlockchainService = ethereumBlockchainService;
        this.purchaseTransactionsService = purchaseTransactionsService;
        this.userProfileService = userProfileService;
        this.icoService = icoService;
        this.rateService = rateService;
    }

    public void processTransactions() {
        try {
            processBitcoinTransactions();
        } catch (Exception ex) {
            Logger.error("exception during processing bitcoin transactions", ex);
        }

        try {
            processLitecoinTransactions();
        } catch (Exception ex) {
            Logger.error("exception during processing litecoin transactions", ex);
        }
        processEthereumTransactions();
    }

    private void processBitcoinTransactions() throws IOException {
        processBtcLikeServiceTransactions(bitcoinBlockchainService);
    }

    private void processLitecoinTransactions() throws IOException {
        processBtcLikeServiceTransactions(litecoinBlockchainService);
    }


    private void processEthereumTransactions() {
        //get all user profiles

        //todo optimize
        List<UserProfile> profiles = userProfileService.getAllProfiles();
        profiles.forEach(profile -> {

            long start = System.currentTimeMillis();

            String ethAddress = profile.getEthAddress();
            try {
                List<EthTransaction> transactions = ethereumBlockchainService.getTransactions(ethAddress,
                        1, 10000);
                transactions.forEach(ethTransaction -> {
                    processEthTransaction(profile, ethTransaction);
                });
                //if we are exceeding rate limit (5 requests/s) we sleep a little bit
                long remainingTime = 200 - System.currentTimeMillis() + start;
                if(remainingTime > 0) {
                    Thread.sleep(remainingTime);
                }
            } catch (Exception e) {
                Logger.error("exception on processing ethereum transactions", e);
            }
        });
    }

    private String lastProcessedTransaction = "";

    private void processBtcLikeServiceTransactions(BtcLikeBlockchainService blockchainService) throws IOException {
        int offset = 0;
        int count = 3;

        String startTransaction = getStartTransactionId(blockchainService);
        boolean allProcessed = processBtcLikeTransactions(blockchainService, lastProcessedTransaction, offset, count);

        while (!allProcessed) {
            offset += count;
            allProcessed = processBtcLikeTransactions(blockchainService, lastProcessedTransaction, offset, count);
        }
        lastProcessedTransaction = startTransaction;
    }

    private String getStartTransactionId(BtcLikeBlockchainService blockchainService) {
        List<Transaction> transactions = blockchainService.listTransactions(0, 1);
        if(transactions.size() == 0) {
            return "";
        } else {
            return transactions.get(0).getTxid();
        }

    }

    private boolean processBtcLikeTransactions(BtcLikeBlockchainService service, String lastProcessedTransaction,
                                               int offset, int count) throws IOException {

        List<Transaction> transactions = service.listTransactions(offset, count);

        for (int i = transactions.size() - 1; i >= 0; i--) {
            Transaction transaction = transactions.get(i);

            if(transaction.getTxid().equals(lastProcessedTransaction)) {
                return true;
            }
//            Logger.debug("checking if {} transaction with id {} is processed", service.getType(), transaction.getTxid());
            if (!purchaseTransactionsService.isTransactionProcessed(service.getType(), transaction.getTxid())) {
                Logger.info("processing new {} transaction {}", service.getType(), transaction.getTxid());
                processBtcLikeTransaction(service.getType(), transaction);
            }
        }
        if(transactions.size() < count) {
            return true;
        }
        return false;
    }

    private void processEthTransaction(UserProfile userProfile, EthTransaction transaction) {
        PurchaseTransaction purchaseTransaction = new PurchaseTransaction();

        String receiverAddress = userProfile.getEthAddress();

        if(transaction.confirmations >= ETHEREUM_TRANSACTION_CONFIRMATIONS && receiverAddress.equals(transaction.to)) {
            if(!purchaseTransactionsService.isTransactionProcessed(PurchaseTransaction.Type.ETH, transaction.hash)) {
                System.out.println("processing transaction " + transaction);
                purchaseTransaction.setSuccess(transaction.isError == 0);
                purchaseTransaction.setType(PurchaseTransaction.Type.ETH);
                purchaseTransaction.setHash(transaction.hash);
                purchaseTransaction.setAmount(BigInteger.valueOf(transaction.value));
                purchaseTransaction.setRate(BigInteger.ONE);
                purchaseTransaction.setWeiAmount(BigInteger.valueOf(transaction.value));
                purchaseTransaction.setUserProfileId(userProfile.getId());
                purchaseTransaction.setTimestamp(transaction.timeStamp);

                if(purchaseTransaction.isSuccess()) {
                    icoService.makePurchase(userProfile, purchaseTransaction);
                }

                purchaseTransactionsService.save(purchaseTransaction);
            }
        }
    }

    private void processBtcLikeTransaction(PurchaseTransaction.Type type, Transaction transaction) throws IOException {
        PurchaseTransaction purchaseTransaction = new PurchaseTransaction();

        if(transaction.getConfirmations() >= BTC_LIKE_BLOCKCHAIN_TRANSACTION_CONFIRMATIONS) {

            //find profile by address
            Optional<UserProfile> profileByAddress;
            if(type == PurchaseTransaction.Type.BTC) {
                profileByAddress = userProfileService.findByBtcAddress(transaction.getAddress());
            } else {
                profileByAddress = userProfileService.findByLtcAddress(transaction.getAddress());
            }

            //todo check transaction not failed
            purchaseTransaction.setSuccess(true);
            purchaseTransaction.setType(type);
            purchaseTransaction.setHash(transaction.getTxid());
            purchaseTransaction.setAmount(transaction.getAmount().multiply(BigDecimal.valueOf(100_000_000)).toBigInteger());
            purchaseTransaction.setProcessedAt(LocalDateTime.now());
            purchaseTransaction.setTimestamp(transaction.getTime());

            if (profileByAddress.isPresent()) {
                BigDecimal rate;

                Timestamp timestamp = Timestamp.from(Instant.ofEpochSecond(transaction.getTime()));
                RateRecord rates = rateService.getRatesForTime(timestamp);
                if(type == PurchaseTransaction.Type.BTC) {
                    rate = rates.getBtcRate();
                } else if(type == PurchaseTransaction.Type.LTC){
                    rate = rates.getLtcRate();
                } else {
                    throw new RuntimeException("unsupported type " + type);
                }

                purchaseTransaction.setRate(rate.multiply(BigDecimal.valueOf(Math.pow(10, 6))).toBigInteger());
                BigInteger amountInWei = rate.multiply(transaction.getAmount())
                        .multiply(BigDecimal.valueOf(IcoInfo.WEI_MULTIPLIER.longValue()))
                        .toBigInteger();

                purchaseTransaction.setWeiAmount(amountInWei);
                purchaseTransaction.setUserProfileId(profileByAddress.get().getId());
                icoService.makePurchase(profileByAddress.get(), purchaseTransaction);
            }
            purchaseTransactionsService.save(purchaseTransaction);
        }
    }
}
