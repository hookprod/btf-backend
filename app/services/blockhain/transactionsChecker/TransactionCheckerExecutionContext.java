package services.blockhain.transactionsChecker;

import akka.actor.ActorSystem;
import play.libs.concurrent.CustomExecutionContext;

import javax.inject.Inject;

/**
 * Created by Gleb Zykov on 29/01/2018.
 * exection context to run processing transactions service
 */
public class TransactionCheckerExecutionContext extends CustomExecutionContext {

    @Inject
    public TransactionCheckerExecutionContext(ActorSystem actorSystem) {
        super(actorSystem, "transaction-checker-dispatcher");
    }

}
