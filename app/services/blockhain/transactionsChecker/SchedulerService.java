package services.blockhain.transactionsChecker;

import akka.actor.ActorSystem;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import play.Logger;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

/**
 * Created by kataloo on 7/17/17.
 * service to schedule running transaction processings
 */
@Singleton
public class SchedulerService {

    private final TransactionsHandlerService transactionsHandlerService;

    @Inject
    public SchedulerService(TransactionsHandlerService transactionsHandlerService,
                            ActorSystem actorSystem,
                            TransactionCheckerExecutionContext transactionCheckerExecutionContext) {
        this.transactionsHandlerService = transactionsHandlerService;
        if(actorSystem.scheduler() == null) {
            return; // for tests
        }
        actorSystem.scheduler().schedule(Duration.Zero(),
                Duration.create(15, TimeUnit.SECONDS),
                this::run,
                transactionCheckerExecutionContext);
    }

    public void run() {
        Logger.info("running checker service");
        try {
            transactionsHandlerService.processTransactions();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
