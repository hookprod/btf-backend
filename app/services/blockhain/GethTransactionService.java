package services.blockhain;

import java.math.BigInteger;

/**
 * Created by Gleb Zykov on 22/01/2018.
 * send transaction with get
 */
public class GethTransactionService {

    private static final String methodSha = "0xa9059cbb";

    public String formParameters(String address, BigInteger value) {
        return methodSha + encodeAddress(address) + encodeUint256(value);
    }

    public static  String encodeAddress(String address) {
        address = address.substring(2, address.length());
        int length = address.length();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < 64 - length; i++) {
            sb.append("0");
        }
        sb.append(address);
        return sb.toString();
    }

    public static String encodeUint256(BigInteger value) {
        return String.format("%064X", value);
    }

}
