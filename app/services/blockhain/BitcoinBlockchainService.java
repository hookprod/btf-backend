package services.blockhain;

import bcJsonRpc.BitcoindInterface;
import bcJsonRpc.events.AlertListener;
import bcJsonRpc.events.BlockListener;
import bcJsonRpc.events.WalletListener;
import bcJsonRpc.pojo.Block;
import bcJsonRpc.pojo.Transaction;
import model.PurchaseTransaction;
import play.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.List;

/**
 * Blockchain service
 */
@Singleton
public class BitcoinBlockchainService implements BtcLikeBlockchainService {

    private BitcoindInterface client;

    @Inject
    public BitcoinBlockchainService(BitcoindInterface client) {
        this.client = client;
    }

    public String generateAddress() {
        return client.getnewaddress("");
    }

    @Override
    public PurchaseTransaction.Type getType() {
        return PurchaseTransaction.Type.BTC;
    }

    public List<Transaction> listTransactions(int offset, int count) {
        List<Transaction> transactions = client.listtransactions("", count, offset);
        return transactions;
    }

    private void start() {
        Logger.info("starting updates listeners");
        try {
            new BlockListener(client).addObserver((o, arg) -> {
                Block block = (Block)arg;
                System.out.println("got block " + block);
            });

            new WalletListener(client).addObserver((o, arg) -> {
                System.out.println("got wallet update " + arg);
                if(arg != null && arg instanceof Transaction) {
                    Transaction transaction = (Transaction) arg;
//                    transaction.getDetails().forEach(this::handleTransactionUpdate);
                }
            });

            new AlertListener().addObserver((o, arg) -> {
                System.out.println("got alert " + arg);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleTransactionUpdate(Transaction transaction) {
        if(transaction != null && transaction.getAccount() != null) {
            try {
                String orderId = transaction.getAccount();
                
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
        }
    }
}
