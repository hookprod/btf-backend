package services.blockhain;

import bcJsonRpc.pojo.Transaction;
import model.PurchaseTransaction;

import java.util.List;

/**
 * Created by Gleb Zykov on 18/01/2018.
 */
public interface BtcLikeBlockchainService extends BlockchainService {

    PurchaseTransaction.Type getType();

    List<Transaction> listTransactions(int offset, int count);

}
