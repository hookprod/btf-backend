package services.blockhain;

import bcJsonRpc.BitcoindInterface;
import model.PurchaseTransaction;
import services.blockhain.BitcoinBlockchainService;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Gleb Zykov on 17/01/2018.
 * Service for operations with litecoin
 */
@Singleton
public class LitecoinBlockchainService extends BitcoinBlockchainService {

    @Inject
    public LitecoinBlockchainService(LitecoindInterface client) {
        super(client);
    }

    @Override
    public PurchaseTransaction.Type getType() {
        return PurchaseTransaction.Type.LTC;
    }

}
