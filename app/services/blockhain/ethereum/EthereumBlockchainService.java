package services.blockhain.ethereum;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.kevinsawicki.http.HttpRequest;
import com.typesafe.config.Config;
import play.Logger;
import play.libs.Json;
import services.blockhain.BlockchainService;
import services.blockhain.GethTransactionService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by Gleb Zykov on 10/12/2017.
 *
 */
@Singleton
public class EthereumBlockchainService implements BlockchainService {

    private static String URL;

    private final GethTransactionService gethTransactionService;

    //wallet address used to run transactions
    private final String ACCOUNT_TO_RUN_TRANSACTIONS = "0xb086f878bbf8f821d6128bbc80981244c38cc970";

    //token address
    private final String TOKEN_ADDRESS;

    @Inject
    public EthereumBlockchainService(Config config, GethTransactionService gethTransactionService) throws IOException {
        this.gethTransactionService = gethTransactionService;
        URL = config.getString("geth.url");
        TOKEN_ADDRESS = config.getString("tokenAddress");

    }

    public String sendTokens(String to, BigInteger amount) {
        return sendTransaction(gethTransactionService.formParameters(to, amount));
    }

    private String sendTransaction(String encodedParameters) {
        HttpRequest request = HttpRequest.post(URL)
                .header("Content-Type", "application/json");

        TransactionParams params = new TransactionParams(ACCOUNT_TO_RUN_TRANSACTIONS, TOKEN_ADDRESS, encodedParameters);

        String requestBody = Json.toJson(new GethRpcRequest("eth_sendTransaction", params)).toString();

        Logger.info("sending request to geth: {}", requestBody);
        String response = request.send(requestBody)
                .body();
        Logger.info("response is: " + response);
        StringResponse result = getResult(response, StringResponse.class);
        return result.result;
    }

    private BigDecimal getBalance(String address, String blockNumberDescription) {
        HttpRequest request = HttpRequest.post(URL)
                .header("Content-Type", "application/json");
        String requestBody = Json.toJson(new GethRpcRequest("eth_getBalance",
                address, blockNumberDescription)).toString();
        String response = request.send(requestBody)
                .body();
        return BigDecimal.valueOf(getResult(response, LongResponse.class).getResult());
    }

    public long getBlockNumber() {
        GethRpcRequest request = new GethRpcRequest("eth_blockNumber");
        String s = Json.toJson(request).toString();
        String response = HttpRequest.post(URL)
                .header("Content-Type", "application/json")
                .send(s)
                .body();
        System.out.println("Block number response: " + response);
        return getResult(response, LongResponse.class).getResult();
    }

    public List<EthTransaction> getTransactions(String address, int page, int size) throws IOException {
        String response = HttpRequest.get("https://rinkeby.etherscan.io/api?module=account" +
                "&action=txlist" +
                "&address=" + address +
                "&startblock=0" +
                "&endblock=99999999" +
                "&page=" + page +
                "&offset=" + size +
                "&sort=asc" +
                "&apikey=YourApiKeyToken")
                .body();
        EthTransactionResponse result = getResult(response, EthTransactionResponse.class);
        return result.result;
    }

    public String generateAddress() {
        String requestBody = Json.toJson(new GethRpcRequest("personal_newAccount",
                "password")).toString();
        String response = HttpRequest.post(URL)
                .header("Content-Type", "application/json")
                .send(requestBody)
                .body();
        System.out.println("create account response: " + response);
        return  getResult(response, StringResponse.class).result;
    }

    private <T> T getResult(String body, Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode jsonNode = mapper.readTree(body);
            return Json.fromJson(jsonNode, clazz);
        } catch (JsonParseException e) {
            Logger.error("could not parse answer {}:\n{}", body, e.getMessage());
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * check if transaction was successfully mined
     * @param transaction hash of transaction to check
     * @return false if transaction not found or it was unsuccessful
     */
    public boolean isTransactionSuccessfulAndConfirmed(String transaction) {
        HttpRequest request = HttpRequest.post(URL)
                .header("Content-Type", "application/json");
        String requestBody = Json.toJson(new GethRpcRequest("eth_getTransactionReceipt",
                transaction)).toString();
        String response = request.send(requestBody)
                .body();
        Logger.info("get transaction response is {}", response);
        TransactionReceiptResponse result = getResult(response, TransactionReceiptResponse.class);
        if(result.result == null) {
            return false;
        }
        if(result.result.isSuccess()) {
            return true;
        } else {
            return false;
        }
    }
}
