package services.blockhain.ethereum;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Gleb Zykov on 10/12/2017.
 */
public class GethRpcRequest {

    public final String jsonrpc = "2.0";

    public final String method;

    public final ArrayList<Object> params;

    public final long id = 1;

    public GethRpcRequest(String method, ArrayList<Object> params) {
        this.method = method;
        this.params = params;
    }

    public GethRpcRequest(String method, Object... params) {
        this.method = method;
        ArrayList<Object> paramsList = new ArrayList<>();
        Collections.addAll(paramsList, params);
        this.params = paramsList;
    }

    public GethRpcRequest(String method) {
        this.method = method;
        this.params = new ArrayList<>();
    }
}
