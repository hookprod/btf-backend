package services.blockhain.ethereum;

/**
 * Created by Gleb Zykov on 10/12/2017.
 */
public class LongResponse {

    public String result;

    public long getResult() {
        return Long.decode(result);
    }

}
