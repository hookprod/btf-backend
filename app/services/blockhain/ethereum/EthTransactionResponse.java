package services.blockhain.ethereum;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gleb Zykov on 18/01/2018.
 * Api response adapter
 */
public class EthTransactionResponse {

    public List<EthTransaction> result = new ArrayList<>();

    public int status;

}
