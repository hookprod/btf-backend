package services.blockhain.ethereum;

/**
 * Created by Gleb Zykov on 22/01/2018.
 */
public class TransactionReceipt {

    public String transactionHash;
    public String blockNumber;
    public String status;

    public boolean isSuccess() {
        return "0x1".equals(status);
    }

}
