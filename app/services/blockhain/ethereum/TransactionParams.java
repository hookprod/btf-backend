package services.blockhain.ethereum;

/**
 * Created by Gleb Zykov on 22/01/2018.
 */
public class TransactionParams {

    public String from;

    public String to;

    public String data;

    public TransactionParams(String from, String to, String data) {
        this.from = from;
        this.to = to;
        this.data = data;
    }
}
