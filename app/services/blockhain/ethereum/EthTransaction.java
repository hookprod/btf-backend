package services.blockhain.ethereum;

public class EthTransaction {

    public long timeStamp;

    public String hash;

    public String from;

    public String to;

    public int isError;

    public int nonce;

    public int transactionIndex;

    public int gas;

    public long gasPrice;

    public long gasUsed;

    public long cumulativeGasUsed;

    public int confirmations;

    public long value;


    @Override
    public String toString() {
        return "EthTransaction{" +
                "timeStamp=" + timeStamp +
                ", hash='" + hash + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", isError=" + isError +
                ", nonce=" + nonce +
                ", transactionIndex=" + transactionIndex +
                ", gas=" + gas +
                ", gasPrice=" + gasPrice +
                ", gasUsed=" + gasUsed +
                ", cumulativeGasUsed=" + cumulativeGasUsed +
                ", confirmations=" + confirmations +
                ", value=" + value +
                '}';
    }
}
