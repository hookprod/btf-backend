package services.blockhain;

import bcJsonRpc.BitcoindInterface;

import javax.inject.Singleton;

/**
 * Created by Gleb Zykov on 17/01/2018.
 */
public interface LitecoindInterface extends BitcoindInterface {
}
