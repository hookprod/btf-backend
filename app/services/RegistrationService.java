package services;

import com.google.inject.Inject;
import com.typesafe.config.Config;
import controllers.registration.RegistrationRequest;
import model.PasswordRecovery;
import model.UserProfile;
import org.mindrot.jbcrypt.BCrypt;
import play.db.jpa.JPAApi;
import services.blockhain.BitcoinBlockchainService;
import services.blockhain.ethereum.EthereumBlockchainService;
import services.blockhain.LitecoinBlockchainService;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class RegistrationService {

    private final UserProfileService userProfileService;
    private final JPAApi jpaApi;
    private final UserNotificationService userNotificationService;
    private final BitcoinBlockchainService bitcoinBlockchainService;
    private final LitecoinBlockchainService litecoinBlockchainService;
    private final EthereumBlockchainService ethereumBlockchainService;
    private final SequenceGeneratorService sequenceGeneratorService;
    private final Config config;

    @Inject
    public RegistrationService(JPAApi jpaApi, UserProfileService userProfileService,
                               UserNotificationService userNotificationService,
                               BitcoinBlockchainService bitcoinBlockchainService,
                               LitecoinBlockchainService litecoinBlockchainService,
                               Config config,
                               SequenceGeneratorService sequenceGeneratorService,
                               EthereumBlockchainService ethereumBlockchainService) {
        this.userProfileService = userProfileService;
        this.jpaApi = jpaApi;
        this.config = config;
        this.sequenceGeneratorService = sequenceGeneratorService;
        this.userNotificationService = userNotificationService;
        this.bitcoinBlockchainService = bitcoinBlockchainService;
        this.ethereumBlockchainService = ethereumBlockchainService;
        this.litecoinBlockchainService = litecoinBlockchainService;
    }

    public void newRegistration(RegistrationRequest registrationRequest) {
        UserProfile userProfile = registrationRequest.toUserProfile();

        String referralCode = registrationRequest.getReferralCode();
        if(referralCode != null && !referralCode.isEmpty()) {
            Optional<UserProfile> referralOpt = userProfileService.findByReferralCode(referralCode);
            if(!referralOpt.isPresent()) {
                throw new IllegalArgumentException("User with referral code " + referralCode + " not found");
            }
            userProfile.setInviterId(referralOpt.get().getId());
        }

        //todo check only users that confirmed emails
        if(userProfileService.findByEmail(registrationRequest.getEmail()).isPresent()) {
            throw new IllegalArgumentException("User with this email is already registered");
        }

        userProfile.setVerified(false);
        userProfile.setVerificationToken(sequenceGeneratorService.generateConfirmationToken());

        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            em.persist(userProfile);
        });

        String serverUrl = config.getString("serverUrl");
        String verificationUrl = serverUrl + "/verifyEmail/" + userProfile.getVerificationToken();

        userNotificationService.sendConfirmationEmail(userProfile.getEmail(), verificationUrl);
    }

    public void confirmRegistration(String token) throws IllegalArgumentException {
        Optional<UserProfile> profileOptional = userProfileService.findByVerificationToken(token);

        if(token == null || token.isEmpty()) {
            throw new IllegalArgumentException("verification token should not be empty");
        }

        if(!profileOptional.isPresent()) {
            throw new IllegalArgumentException("user profile not found");
        } else {
            UserProfile userProfile = profileOptional.get();
            if(!token.equals(userProfile.getVerificationToken())) {
                throw new IllegalArgumentException("wrong verification token");
            } else if(!userProfile.isVerified()){
                userProfile.setVerified(true);
                userProfile.setBtcAddress(bitcoinBlockchainService.generateAddress());
                userProfile.setEthAddress(ethereumBlockchainService.generateAddress());
                userProfile.setLtcAddress(litecoinBlockchainService.generateAddress());
                userProfile.setRegistrationTime(System.currentTimeMillis());

                String referralCode = sequenceGeneratorService.generateReferralCode(6);
                for(int i = 0; i < 100; i++) {
                    if (userProfileService.findByReferralCode(referralCode).isPresent()) {
                        referralCode = sequenceGeneratorService.generateReferralCode(6);
                    } else {
                        break;
                    }
                }

                if(userProfileService.findByReferralCode(referralCode).isPresent()) {
                    throw new RuntimeException("generation of referral code failed after 100 attempts. Please try again later");
                }
                userProfile.setReferralCode(referralCode);
                jpaApi.withTransaction(() -> jpaApi.em().merge(userProfile));
            }
        }
    }

    /**
     * returns user profile on successful auth, throws exception otherwise
     * @param email
     * @param password
     * @return
     * @throws IllegalAccessException
     */
    public UserProfile checkPassword(String email, String password) throws IllegalAccessException {
        Optional<UserProfile> profileOptional = userProfileService.findByEmail(email);
        if(!profileOptional.isPresent()) {
            throw new IllegalAccessException("wrong login or password");
        } else {
            UserProfile userProfile = profileOptional.get();
            if(!BCrypt.checkpw(password, userProfile.getPasswordHash())) {
                throw new IllegalAccessException("wrong login or password");
            }
            return userProfile;
        }
    }

    public void recoverPassword(String userId) {
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            PasswordRecovery saved = em.find(PasswordRecovery.class, userId);
            String recoveryToken = sequenceGeneratorService.generateConfirmationToken();
            if(saved != null) {
                saved.setLastRecoveryRequestTime(System.currentTimeMillis());
                saved.setRecoveryToken(recoveryToken);
                em.merge(saved);
            } else {
                PasswordRecovery passwordRecovery = new PasswordRecovery();
                passwordRecovery.setUserProfileId(userId);
                passwordRecovery.setLastRecoveryRequestTime(System.currentTimeMillis());
                passwordRecovery.setRecoveryToken(recoveryToken);
                em.persist(passwordRecovery);
            }

            UserProfile userProfile = em.find(UserProfile.class, userId);


            String recoveryUrl = config.getString("passwordRecoveryUrl") + "/recovery?token=" +
                    recoveryToken;
            userNotificationService.sendPasswordRecoveryEmail(userProfile.getEmail(), recoveryUrl);
        });
    }

    public void changePassword(String password, String recoveryToken) {
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();

            if(recoveryToken == null || recoveryToken.isEmpty()) {
                throw new IllegalArgumentException("wrong recovery token");
            }

            TypedQuery<PasswordRecovery> query = em.createQuery(
                    "from PasswordRecovery where recoveryToken = :token", PasswordRecovery.class);
            query.setParameter("token", recoveryToken);
            List<PasswordRecovery> resultList = query.getResultList();
            if(resultList.isEmpty()) {
                throw new IllegalArgumentException("wrong recovery token");
            }

            PasswordRecovery passwordRecovery = resultList.get(0);
            if(passwordRecovery == null) {
                throw new IllegalArgumentException("wrong recovery token");
            }

            if(!passwordRecovery.getRecoveryToken().equals(recoveryToken)) {
                throw new IllegalArgumentException("wrong recovery token");
            }

            Long lastRecoveryRequestTime = passwordRecovery.getLastRecoveryRequestTime();
            if(System.currentTimeMillis() - lastRecoveryRequestTime > TimeUnit.HOURS.toMillis(24)) {
                throw new IllegalArgumentException("recovery token is expired. Please request a new one.");
            }

            UserProfile userProfile = em.find(UserProfile.class, passwordRecovery.getUserProfileId());
            userProfile.setPasswordHash(BCrypt.hashpw(password, BCrypt.gensalt()));
            em.merge(userProfile);

            passwordRecovery.setLastRecoveryRequestTime(0L);
            em.merge(passwordRecovery);
        });
    }

}
