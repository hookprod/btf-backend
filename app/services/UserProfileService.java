package services;

import controllers.profile.EditProfileRequest;
import model.UserProfile;
import org.mindrot.jbcrypt.BCrypt;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * Created by Gleb Zykov on 15/01/2018.
 * user profile service
 */
public class UserProfileService {

    private final JPAApi jpaApi;

    @Inject
    public UserProfileService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public Optional<UserProfile> findById(String id) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<UserProfile> query =
                    em.createQuery("SELECT p from UserProfile p where p.id = :id", UserProfile.class);
            query.setParameter("id", id);
            List<UserProfile> resultList = query.getResultList();
            if(resultList.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(resultList.get(0));
            }
        });
    }

    public Optional<UserProfile> findByEmail(String email) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<UserProfile> query =
                    em.createQuery("SELECT p from UserProfile p where p.email = :email and p.isVerified = true",
                            UserProfile.class);
            query.setParameter("email", email);
            List<UserProfile> resultList = query.getResultList();
            if(resultList.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(resultList.get(0));
            }
        });
    }

    public Optional<UserProfile> findByReferralCode(String referralCode) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<UserProfile> query =
                    em.createQuery("SELECT p from UserProfile p where p.referralCode = :referralCode and p.isVerified = true",
                            UserProfile.class);
            query.setParameter("referralCode", referralCode);
            List<UserProfile> resultList = query.getResultList();
            if(resultList.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(resultList.get(0));
            }
        });
    }

    public Optional<UserProfile> findByVerificationToken(String token) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<UserProfile> query =
                    em.createQuery("SELECT p from UserProfile p where p.verificationToken = :token", UserProfile.class);
            query.setParameter("token", token);
            List<UserProfile> resultList = query.getResultList();
            if(resultList.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(resultList.get(0));
            }
        });
    }

    public void setWalletAddress(String userId, String erc20walletAddress) {
        Optional<UserProfile> profileOptional = findById(userId);
        if(!profileOptional.isPresent()) {
            throw new IllegalArgumentException("no profile with such id");
        }
        UserProfile userProfile = profileOptional.get();
        userProfile.setErc20WalletAddress(erc20walletAddress);
        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            em.merge(userProfile);
        });
    }

    public List<UserProfile> getAllProfiles() {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<UserProfile> query = em.createQuery("from UserProfile p where p.isVerified = true",
                    UserProfile.class);
            return query.getResultList();
        });
    }

    public Optional<UserProfile> findByBtcAddress(String address) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<UserProfile> query =
                    em.createQuery("SELECT p from UserProfile p where p.btcAddress = :address", UserProfile.class);
            query.setParameter("address", address);
            List<UserProfile> resultList = query.getResultList();
            if(resultList.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(resultList.get(0));
            }
        });
    }

    public Optional<UserProfile> findByLtcAddress(String address) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<UserProfile> query =
                    em.createQuery("SELECT p from UserProfile p where p.ltcAddress = :address", UserProfile.class);
            query.setParameter("address", address);
            List<UserProfile> resultList = query.getResultList();
            if(resultList.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(resultList.get(0));
            }
        });
    }

    public UserProfile editUser(String userId, EditProfileRequest editProfileRequest) {
        Optional<UserProfile> userProfileOpt = findById(userId);
        if(!userProfileOpt.isPresent()) {
            throw new IllegalArgumentException("user with id " + userId + "not found");
        }

        return jpaApi.withTransaction(() -> {
            UserProfile userProfile = userProfileOpt.get();

            userProfile.setFirstName(editProfileRequest.getFirstName());
            userProfile.setLastName(editProfileRequest.getLastName());
            userProfile.setPhoneNumber(editProfileRequest.getPhoneNumber());


            if(editProfileRequest.getNewPassword() != null &&
                    !editProfileRequest.getNewPassword().isEmpty()) {

                if(!BCrypt.checkpw(editProfileRequest.getOldPassword(), userProfile.getPasswordHash())) {
                    throw new IllegalArgumentException("wrong password");
                }

                userProfile.setPasswordHash(BCrypt.hashpw(editProfileRequest.getNewPassword(), BCrypt.gensalt()));
            }
            jpaApi.em().merge(userProfile);
            return userProfile;
        });
    }

    public List<UserProfile> getReferrals(String userId, int page, int size) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<UserProfile> query = em.createQuery(
                    "from UserProfile where inviterId = :id and isVerified = true", UserProfile.class);
            query.setParameter("id", userId);
            query.setFirstResult((page - 1) * size);
            query.setMaxResults(size);
            return query.getResultList();
        });
    }

    public long getReferralsCount(String userId) {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            Query query = em.createQuery("select count (*) from UserProfile where inviterId = :id");
            query.setParameter("id", userId);
            return (long) query.getSingleResult();
        });
    }
}
