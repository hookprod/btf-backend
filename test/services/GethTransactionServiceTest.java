package services;

import org.junit.Test;
import services.blockhain.GethTransactionService;

import java.math.BigInteger;

import static org.junit.Assert.*;

/**
 * Created by Gleb Zykov on 22/01/2018.
 */
public class GethTransactionServiceTest {

    @Test
    public void encodeAddress() throws Exception {
        assertEquals("000000000000000000000000407d73d8a49eeb85d32cf465507dd71d507100c1",
                GethTransactionService.encodeAddress("0x407d73d8a49eeb85d32cf465507dd71d507100c1"));
    }

    @Test
    public void encodeUint256() throws Exception {
        assertEquals("1 should be correctly encoded", "0000000000000000000000000000000000000000000000000000000000000001",
                GethTransactionService.encodeUint256(BigInteger.ONE));

        assertEquals("16 should be correctly encoded", "0000000000000000000000000000000000000000000000000000000000000010",
                GethTransactionService.encodeUint256(BigInteger.valueOf(16L)));

    }

}