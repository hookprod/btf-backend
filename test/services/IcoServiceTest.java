package services;

import model.IcoInfo;
import model.PurchaseTransaction;
import model.Stage;
import org.junit.Test;
import org.mockito.Mockito;
import play.db.jpa.JPAApi;

import java.math.BigInteger;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

/**
 * Created by Gleb Zykov on 17/01/2018.
 */
public class IcoServiceTest {

    @Test
    public void testUpdateIcoInfo() {
        IcoService icoService = new IcoService(Mockito.mock(JPAApi.class));
        IcoInfo icoInfo = new IcoInfo();
        icoInfo.setPreIcoStartTime(LocalDateTime.now().minusDays(1));

        icoService.updateIcoInfo(icoInfo, BigInteger.valueOf(1000), BigInteger.TEN, BigInteger.ONE);
        assertEquals(icoInfo.getSoldTokens(), BigInteger.valueOf(1000));
        assertEquals(icoInfo.getBonusTokens(), BigInteger.TEN);
        assertEquals(icoInfo.getReferralTokens(), BigInteger.ONE);
    }

    @Test
    public void testUpdateIco() {
        IcoService icoService = new IcoService(Mockito.mock(JPAApi.class));
        IcoInfo icoInfo = new IcoInfo();

        icoInfo.setPreIcoStartTime(LocalDateTime.now().minusDays(1));

        icoService.updateIcoInfo(icoInfo, BigInteger.valueOf(20_000_000).multiply(IcoInfo.TOKEN_MULTIPLIER),
                BigInteger.valueOf(10_000_000).multiply(IcoInfo.TOKEN_MULTIPLIER), BigInteger.ZERO);

        Stage currentStage = icoInfo.getCurrentStage();

        assertEquals("should move to next stage(first ico stage)", 30, currentStage.getBonusPercent());
        assertEquals(icoInfo.getSoldDuringPreIco(), BigInteger.valueOf(30_000_000).multiply(IcoInfo.TOKEN_MULTIPLIER));
        assertNotNull(icoInfo.getIcoStartTime());
        assertTrue(icoInfo.getIcoStartTime().equals(LocalDateTime.now()));
    }

    @Test
    public void getStageFor() {
        IcoInfo icoInfo = new IcoInfo();

        Stage stage;
        stage = icoInfo.getIcoStage(BigInteger.valueOf(100).multiply(IcoInfo.TOKEN_MULTIPLIER), BigInteger.ZERO,
                BigInteger.ZERO);

        assertEquals("percent on first stage should be 30", 30, stage.getBonusPercent());

        stage = icoInfo.getIcoStage(BigInteger.valueOf(13_000_000 - 1).multiply(IcoInfo.TOKEN_MULTIPLIER), BigInteger.ZERO,
                BigInteger.ZERO);
        assertEquals("percent on first stage should be 30", 30, stage.getBonusPercent());

        stage = icoInfo.getIcoStage(BigInteger.valueOf(13_000_000).multiply(IcoInfo.TOKEN_MULTIPLIER), BigInteger.ZERO,
                BigInteger.ZERO);
        assertEquals("percent on first stage should be 20", 20, stage.getBonusPercent());

        stage = icoInfo.getIcoStage(BigInteger.valueOf(13_000_001).multiply(IcoInfo.TOKEN_MULTIPLIER), BigInteger.ZERO,
                BigInteger.ZERO);
        assertEquals("percent on first stage should be 20", 20, stage.getBonusPercent());

        stage = icoInfo.getIcoStage(BigInteger.valueOf(37_000_001).multiply(IcoInfo.TOKEN_MULTIPLIER), BigInteger.ZERO,
                BigInteger.ZERO);

        assertEquals("percent on first stage should be 10", 10, stage.getBonusPercent());

        stage = icoInfo.getIcoStage(BigInteger.valueOf(70_000_001).multiply(IcoInfo.TOKEN_MULTIPLIER), BigInteger.ZERO,
                BigInteger.ZERO);

        assertEquals("percent on first stage should be 0", 0, stage.getBonusPercent());
    }

    @Test
    public void testCalculateBoughtTokens() {
        PurchaseTransaction purchaseTransaction = new PurchaseTransaction();
        purchaseTransaction.setWeiAmount(IcoInfo.WEI_MULTIPLIER);

        JPAApi jpaApiMock = Mockito.mock(JPAApi.class);
        IcoService icoService = new IcoService(jpaApiMock);
        BigInteger tokens = icoService.calculateBoughtTokens(purchaseTransaction);
        assertEquals("should return correct tokens value",
                IcoInfo.BASE_TOKEN_PRICE.multiply(IcoInfo.TOKEN_MULTIPLIER), tokens);
    }


    @Test
    public void testCalculateBonuses() {
        JPAApi jpaApiMock = Mockito.mock(JPAApi.class);
        IcoService icoService = new IcoService(jpaApiMock);

        BigInteger bonuses = icoService.calculateBonuses(BigInteger.TEN, 10);

        assertEquals(BigInteger.valueOf(1), bonuses);
    }


}