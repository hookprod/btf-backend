package services;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Gleb Zykov on 23/01/2018.
 */
public class JwtServiceTest {

    @Test
    public void generateJwt() throws Exception {
        JwtService jwtService = new JwtService();
        String secret = "secret";

        String jwt = jwtService.generateJwt(secret, "mike");
        System.out.println("generated jwt: " + jwt);
        String subject = jwtService.getSubject(jwt, secret);
        assertEquals("encoded signature should be correctly parsed", "mike", subject);

    }

}