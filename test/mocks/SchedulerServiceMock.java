package mocks;

import akka.actor.ActorSystem;
import org.mockito.Mockito;
import services.blockhain.transactionsChecker.SchedulerService;
import services.blockhain.transactionsChecker.TransactionCheckerExecutionContext;
import services.blockhain.transactionsChecker.TransactionsHandlerService;

/**
 * Created by Gleb Zykov on 02/02/2018.
 * mock class for tests
 */
public class SchedulerServiceMock extends SchedulerService {

    public SchedulerServiceMock() {
        super(Mockito.mock(TransactionsHandlerService.class),
                Mockito.mock(ActorSystem.class),
                Mockito.mock(TransactionCheckerExecutionContext.class));
    }
}


