package mocks;

import services.MailService;

/**
 * Created by Gleb Zykov on 02/02/2018.
 * mock class for tests
 */
public class MailServiceMock extends MailService {
    @Override
    public void sendMail(String to, String header, String messageText) {
        //do nothing
    }
}
