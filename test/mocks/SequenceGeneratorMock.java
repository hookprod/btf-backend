package mocks;

import services.SequenceGeneratorService;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Gleb Zykov on 02/02/2018.
 * mock class for tests
 */
public class SequenceGeneratorMock extends SequenceGeneratorService {

    private AtomicInteger counter = new AtomicInteger(1);

    @Override
    public String generateConfirmationToken() {
        return "xxx" + counter.getAndIncrement();
    }

}
