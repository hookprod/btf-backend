package mocks;

import bcJsonRpc.BitcoindInterface;
import bcJsonRpc.pojo.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by Gleb Zykov on 02/02/2018.
 * mock class for tests
 */
public class BitcoinServiceMock implements BitcoindInterface {
    @Override
    public String addmultisigaddress(int nrequired, String keys) {
        return null;
    }

    @Override
    public String addmultisigaddress(int nrequired, String keys, String account) {
        return null;
    }

    @Override
    public Info getinfo() {
        return null;
    }

    @Override
    public boolean backupwallet() {
        return false;
    }

    @Override
    public void walletlock() {

    }

    @Override
    public boolean settxfee(BigDecimal fee) {
        return false;
    }

    @Override
    public boolean setaccount(String bitcoinAddress, String accountLabel) {
        return false;
    }

    @Override
    public String getaccount(String bitcoinAddress) {
        return null;
    }

    @Override
    public String getaccountaddress(String accountLabel) {
        return null;
    }

    @Override
    public List<String> getaddressesbyaccount(String accountLabel) {
        return null;
    }

    @Override
    public BigDecimal getbalance() {
        return null;
    }

    @Override
    public BigDecimal getbalance(String account) {
        return null;
    }

    @Override
    public BigDecimal getbalance(String account, int minimumConfirmations) {
        return null;
    }

    @Override
    public Block getblock(String blockHash) {
        return null;
    }

    @Override
    public long getblockcount() {
        return 0;
    }

    @Override
    public String getblockhash(long blockHeight) {
        return null;
    }

    @Override
    public int getconnectioncount() {
        return 0;
    }

    @Override
    public BigDecimal getdifficulty() {
        return null;
    }

    @Override
    public boolean getgenerate() {
        return false;
    }

    @Override
    public long gethashespersec() {
        return 0;
    }

    @Override
    public Transaction gettransaction(String hash) {
        return null;
    }

    @Override
    public Map<String, BigDecimal> listaccounts(long confirmations) {
        return null;
    }

    @Override
    public List<Account> listreceivedbyaccount(long minConfirmations, boolean includeEmpty) {
        return null;
    }

    @Override
    public List<Address> listreceivedbyaddress(long minConfirmations, boolean includeEmpty) {
        return null;
    }

    @Override
    public List<LastBlock> listsinceblock(String blockhash, int minConfirmations) {
        return null;
    }

    @Override
    public List<Transaction> listtransactions(String account, int count, int offset) {
        return null;
    }

    @Override
    public boolean importprivkey(String privateKey) {
        return false;
    }

    @Override
    public boolean move(String fromAccount, String toAccount, BigDecimal amount) {
        return false;
    }

    @Override
    public boolean move(String fromAccount, String toAccount, BigDecimal amount, long minconf, String comment) {
        return false;
    }

    @Override
    public String sendfrom(String fromAccount, String bitcoinAddress, BigDecimal amount) {
        return null;
    }

    @Override
    public String sendfrom(String fromAccount, String bitcoinAddress, BigDecimal amount, long minconf, String comment, String commentTo) {
        return null;
    }

    @Override
    public String sendmany(String fromAccount, Map<String, BigDecimal> addressAmountPairs) {
        return null;
    }

    @Override
    public String sendmany(String fromAccount, Map<String, BigDecimal> addressAmountPairs, int minconf, String comment) {
        return null;
    }

    @Override
    public String sendtoaddress(String bitcoinAddress, BigDecimal amount) {
        return null;
    }

    @Override
    public void setgenerate(boolean generate) {

    }

    @Override
    public void setgenerate(boolean generate, int genproclimit) {

    }

    @Override
    public AddressInformation validateaddress(String bitcoinAddress) {
        return null;
    }

    @Override
    public String getnewaddress(String label) {
        return null;
    }

    @Override
    public String signmessage(String bitcoinaddress, String message) {
        return null;
    }

    @Override
    public boolean verifymessage(String bitcoinaddress, String signature, String message) {
        return false;
    }

    @Override
    public String stop() {
        return null;
    }
}
