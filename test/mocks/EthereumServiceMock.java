package mocks;

import com.typesafe.config.Config;
import org.mockito.Mockito;
import services.blockhain.GethTransactionService;
import services.blockhain.ethereum.EthereumBlockchainService;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Gleb Zykov on 02/02/2018.
 * mock class for tests
 */
public class EthereumServiceMock extends EthereumBlockchainService {

    private AtomicInteger addressSequence = new AtomicInteger(1);

    public EthereumServiceMock() throws IOException {
        super(Mockito.mock(Config.class),
                Mockito.mock(GethTransactionService.class));
    }

    @Override
    public String generateAddress() {
        return "0x0" + addressSequence.getAndIncrement();
    }
}
