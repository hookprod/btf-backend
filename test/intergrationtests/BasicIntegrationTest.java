package intergrationtests;

import bcJsonRpc.BitcoindInterface;
import mocks.*;
import model.IcoInfo;
import model.PurchaseTransaction;
import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.WithApplication;
import services.MailService;
import services.SequenceGeneratorService;
import services.blockhain.LitecoindInterface;
import services.blockhain.ethereum.EthereumBlockchainService;
import services.blockhain.transactionsChecker.SchedulerService;
import static org.junit.Assert.*;

import java.math.BigInteger;

import static play.inject.Bindings.bind;

/**
 * Created by Gleb Zykov on 02/02/2018.
 * super class for integration tests
 */
public class BasicIntegrationTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder()
                .overrides(bind(SequenceGeneratorService.class).to(SequenceGeneratorMock.class))
                .overrides(bind(MailService.class).to(MailServiceMock.class))
                .overrides(bind(SchedulerService.class).to(SchedulerServiceMock.class))
                .overrides(bind(LitecoindInterface.class).to(LitecoinServiceMock.class))
                .overrides(bind(BitcoindInterface.class).to(BitcoinServiceMock.class))
                .overrides(bind(EthereumBlockchainService.class).to(EthereumServiceMock.class))
                .build();
    }

    BigInteger calculateWeiSumToBuyTokens(BigInteger tokensAmountWithoutDecimals, int bonusPercent) {
        BigInteger eth = IcoInfo.WEI_MULTIPLIER;
        return eth.multiply(tokensAmountWithoutDecimals)
                .divide(IcoInfo.BASE_TOKEN_PRICE)
                .divide(BigInteger.valueOf(100 + bonusPercent))
                .multiply(BigInteger.valueOf(100));

    }

    PurchaseTransaction createPurchase(int tokensAmount) {
        PurchaseTransaction transaction = new PurchaseTransaction();
        transaction.setSuccess(true);
        BigInteger amount = calculateWeiSumToBuyTokens(BigInteger.valueOf(tokensAmount), 0);
        transaction.setWeiAmount(amount);
        transaction.setAmount(amount);
        transaction.setType(PurchaseTransaction.Type.ETH);
        return transaction;
    }

    @Test
    public void testCalculateWeiSumToBuyTokens() {
        BigInteger eth = IcoInfo.WEI_MULTIPLIER;
        assertEquals(eth, calculateWeiSumToBuyTokens(IcoInfo.BASE_TOKEN_PRICE, 0));
        assertEquals(eth.divide(BigInteger.valueOf(2)),
                calculateWeiSumToBuyTokens(IcoInfo.BASE_TOKEN_PRICE, 100));
    }


}
