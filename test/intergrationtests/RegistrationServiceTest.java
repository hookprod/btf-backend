package intergrationtests;

import controllers.registration.RegistrationRequest;
import model.UserProfile;
import org.junit.Test;
import services.RegistrationService;
import services.UserProfileService;

import java.util.List;

import static org.junit.Assert.*;

public class RegistrationServiceTest extends BasicIntegrationTest {

    /**
     * add your integration test here
     * in this example we just check if the welcome page is being shown
     */
    @Test
    public void test() {
        UserProfileService userProfileService = instanceOf(UserProfileService.class);
        RegistrationService registrationService = instanceOf(RegistrationService.class);

        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setEmail("test@test.com");
        registrationRequest.setPassword("xxx");
        registrationService.newRegistration(registrationRequest);

        registrationService.confirmRegistration("xxx1");

        List<UserProfile> allProfiles = userProfileService.getAllProfiles();
        assertEquals("one profile should be saved", 1, allProfiles.size());

        UserProfile userProfile = allProfiles.get(0);
        assertTrue("user should be verified", userProfile.isVerified());

        String referralCode = userProfile.getReferralCode();

        RegistrationRequest invitedRegistrationRequest = new RegistrationRequest();
        invitedRegistrationRequest.setEmail("invited@test.com");
        invitedRegistrationRequest.setReferralCode(referralCode);
        invitedRegistrationRequest.setPassword("yyy");
        registrationService.newRegistration(invitedRegistrationRequest);

        List<UserProfile> referrals = userProfileService.getReferrals(userProfile.getId(), 0, 10);
        assertEquals("user should have 0 referrals when not confirmed",0, referrals.size());

        try {
            registrationService.confirmRegistration("xxx");
            fail("exception on checking wrong password was not thrown");
        } catch (Exception ex) {
            //ok
        }
        registrationService.confirmRegistration("xxx2");

        referrals = userProfileService.getReferrals(userProfile.getId(), 0, 10);
        assertEquals("user should have 1 referral after confirmation",1,referrals.size());

        //should throw and exception on try to register with same email that already is verified
        RegistrationRequest invitedRegistrationRequestCopy = new RegistrationRequest();
        invitedRegistrationRequestCopy.setEmail("invited@test.com");
        invitedRegistrationRequestCopy.setPassword("yyy");
        try {
            registrationService.newRegistration(invitedRegistrationRequest);
            fail("exception was not thrown on registering with same email");
        } catch (IllegalArgumentException ex) {
            //ok
        }

    }

}
