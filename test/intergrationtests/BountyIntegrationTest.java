package intergrationtests;

import controllers.registration.RegistrationRequest;
import model.Article;
import model.IcoInfo;
import model.PurchaseTransaction;
import model.UserProfile;
import org.junit.Test;
import services.BonusService;
import services.IcoService;
import services.RegistrationService;
import services.UserProfileService;

import java.math.BigInteger;
import java.util.List;

import static org.junit.Assert.*;

public class BountyIntegrationTest extends BasicIntegrationTest {

    @Test
    public void testReferralTokensAreAssigned() {
        UserProfileService userProfileService = instanceOf(UserProfileService.class);
        RegistrationService registrationService = instanceOf(RegistrationService.class);

        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setEmail("test@test.com");
        registrationRequest.setPassword("xxx");
        registrationService.newRegistration(registrationRequest);

        registrationService.confirmRegistration("xxx1");

        List<UserProfile> allProfiles = userProfileService.getAllProfiles();
        assertEquals("one profile should be saved", 1, allProfiles.size());

        UserProfile userProfile = allProfiles.get(0);
        assertTrue("user should be verified", userProfile.isVerified());

        String referralCode = userProfile.getReferralCode();

        RegistrationRequest invitedRegistrationRequest = new RegistrationRequest();
        invitedRegistrationRequest.setEmail("invited@test.com");
        invitedRegistrationRequest.setReferralCode(referralCode);
        invitedRegistrationRequest.setPassword("yyy");
        registrationService.newRegistration(invitedRegistrationRequest);

        registrationService.confirmRegistration("xxx2");

        PurchaseTransaction transaction = createPurchase(100);

        List<UserProfile> referrals = userProfileService.getReferrals(userProfile.getId(), 0, 10);
        assertEquals("should be 1 referral", 1, referrals.size());
        UserProfile invitedUser = referrals.get(0);

        IcoService icoService = instanceOf(IcoService.class);
        icoService.makePurchase(invitedUser, transaction);

        userProfile = userProfileService.findById(userProfile.getId()).get();

        assertEquals("referral tokens should be assigned",
                BigInteger.valueOf(5).multiply(IcoInfo.TOKEN_MULTIPLIER), userProfile.getReferralTokens());
    }

    @Test
    public void testBonusTokensAreAssigned() {
        UserProfileService userProfileService = instanceOf(UserProfileService.class);
        RegistrationService registrationService = instanceOf(RegistrationService.class);

        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setEmail("test@test.com");
        registrationRequest.setPassword("xxx");
        registrationService.newRegistration(registrationRequest);

        registrationService.confirmRegistration("xxx1");

        List<UserProfile> allProfiles = userProfileService.getAllProfiles();
        assertEquals("one profile should be saved", 1, allProfiles.size());

        UserProfile userProfile = allProfiles.get(0);

        assertEquals(BigInteger.ZERO, userProfile.getBountyTokens());

        BonusService bonusService = instanceOf(BonusService.class);
        bonusService.addSocialBonuses(userProfile.getId(), "FACEBOOK_FOLLOW");

        userProfile = userProfileService.findById(userProfile.getId()).get();
        assertEquals("ten tokens should be assigned",
                BigInteger.TEN.multiply(IcoInfo.TOKEN_MULTIPLIER), userProfile.getBountyTokens());

        try {
            bonusService.addSocialBonuses(userProfile.getId(), "FACEBOOK_FOLLOW");
            fail("exception was not thrown on assigning tokens for same sharing type");
        } catch (RuntimeException ex) {
            //should throw
        }
    }

    @Test
    public void testAssignBountyTokensForArticles() {
        UserProfileService userProfileService = instanceOf(UserProfileService.class);
        RegistrationService registrationService = instanceOf(RegistrationService.class);

        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setEmail("test@test.com");
        registrationRequest.setPassword("xxx");
        registrationService.newRegistration(registrationRequest);

        registrationService.confirmRegistration("xxx1");

        List<UserProfile> allProfiles = userProfileService.getAllProfiles();
        assertEquals("one profile should be saved", 1, allProfiles.size());

        UserProfile userProfile = allProfiles.get(0);

        assertEquals(BigInteger.ZERO, userProfile.getBountyTokens());

        BonusService bonusService = instanceOf(BonusService.class);
        Article article = new Article();
        article.setUserProfile(userProfile);
        article.setTokens(BigInteger.TEN.multiply(IcoInfo.TOKEN_MULTIPLIER));
        article.setUrl("http://vk.com/1");
        bonusService.addArticle(article);

        userProfile = userProfileService.findById(userProfile.getId()).get();

        assertEquals("tokens should not be assigned before approval", BigInteger.ZERO, userProfile.getBountyTokens());

        bonusService.approveArticle(article.getId(), BigInteger.ONE.multiply(IcoInfo.TOKEN_MULTIPLIER));

        userProfile = userProfileService.findById(userProfile.getId()).get();

        assertEquals(BigInteger.ONE.multiply(IcoInfo.TOKEN_MULTIPLIER), userProfile.getBountyTokens());
    }

}
