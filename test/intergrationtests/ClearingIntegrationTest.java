package intergrationtests;

import controllers.registration.RegistrationRequest;
import model.IcoInfo;
import model.PurchaseTransaction;
import model.UserProfile;
import org.junit.Test;
import play.db.jpa.JPAApi;
import services.IcoService;
import services.RegistrationService;
import services.UserProfileService;
import services.clearing.ClearingReportService;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Gleb Zykov on 05/02/2018.
 */
public class ClearingIntegrationTest extends BasicIntegrationTest {

    @Test
    public void testClearingReport() {
        UserProfileService userProfileService = instanceOf(UserProfileService.class);
        RegistrationService registrationService = instanceOf(RegistrationService.class);

        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setEmail("test@test.com");
        registrationRequest.setPassword("xxx");
        registrationService.newRegistration(registrationRequest);

        registrationService.confirmRegistration("xxx1");

        List<UserProfile> allProfiles = userProfileService.getAllProfiles();

        UserProfile userProfile = allProfiles.get(0);
        assertTrue("user should be verified", userProfile.isVerified());

        String referralCode = userProfile.getReferralCode();

        RegistrationRequest invitedRegistrationRequest = new RegistrationRequest();
        invitedRegistrationRequest.setEmail("invited@test.com");
        invitedRegistrationRequest.setReferralCode(referralCode);
        invitedRegistrationRequest.setPassword("yyy");
        registrationService.newRegistration(invitedRegistrationRequest);

        registrationService.confirmRegistration("xxx2");

        PurchaseTransaction transaction = createPurchase(100);

        List<UserProfile> referrals = userProfileService.getReferrals(userProfile.getId(), 0, 10);
        UserProfile invitedUser = referrals.get(0);

        IcoService icoService = instanceOf(IcoService.class);
        icoService.makePurchase(invitedUser, transaction);

        userProfile = userProfileService.findById(userProfile.getId()).get();

        assertEquals("referral tokens should be assigned",
                BigInteger.valueOf(5).multiply(IcoInfo.TOKEN_MULTIPLIER), userProfile.getReferralTokens());

        JPAApi jpaApi = instanceOf(JPAApi.class);

        icoService.manualAddBonusTokens(userProfile.getId(),
                BigInteger.valueOf(10).multiply(IcoInfo.TOKEN_MULTIPLIER), "no comment");

        userProfile = userProfileService.findById(userProfile.getId()).get();
        BigInteger tokensBought = userProfile.getTokensBought();
        assertEquals(BigInteger.valueOf(10).multiply(IcoInfo.TOKEN_MULTIPLIER), tokensBought);
        //check 100% bonus
        assertEquals(BigInteger.valueOf(10).multiply(IcoInfo.TOKEN_MULTIPLIER), userProfile.getBonusTokens());

        UserProfile profileToCheck = userProfile;

        IcoInfo icoInfo = icoService.getIcoInfo();
        assertEquals("referral tokens should be updated in ico info",
                IcoInfo.tokensAmount(5), icoInfo.getReferralTokens());
        assertEquals("bonus tokens should be updated in ico info",
                IcoInfo.tokensAmount(110), icoInfo.getBonusTokens());
        assertEquals("bought tokens should be updated in ico info",
                IcoInfo.tokensAmount(110), icoInfo.getSoldTokens());

        jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            ClearingReportService clearingReportService = instanceOf(ClearingReportService.class);
            BigInteger referralTokens = clearingReportService.getReferralTokens(em, profileToCheck);
            assertEquals(BigInteger.valueOf(5).multiply(IcoInfo.TOKEN_MULTIPLIER),
                    referralTokens);

            BigInteger manualAddedTokens = clearingReportService.getManualBonusesCount(em, profileToCheck);
            assertEquals("purchased tokens should be equal to 10 + 10 with bonus tokens",
                    IcoInfo.tokensAmount(20), manualAddedTokens);

            BigInteger purchasedTokens = clearingReportService.getPurchasedTokens(em, invitedUser);
            assertEquals("should return 100 tokens", IcoInfo.tokensAmount(100), purchasedTokens);

            BigInteger bonusTokens = clearingReportService.getBonusPurchasedTokens(em, invitedUser);
            assertEquals("should be 100 bonus tokens", IcoInfo.tokensAmount(100), bonusTokens);
        });
    }

}
