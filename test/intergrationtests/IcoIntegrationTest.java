package intergrationtests;

import controllers.registration.RegistrationRequest;
import model.IcoInfo;
import model.PurchaseTransaction;
import model.UserProfile;
import org.junit.Test;
import services.IcoService;
import services.RegistrationService;
import services.UserProfileService;
import static org.junit.Assert.*;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Gleb Zykov on 02/02/2018.
 * ico integration test
 */
public class IcoIntegrationTest extends BasicIntegrationTest {

    @Test
    public void test() {
        UserProfileService userProfileService = instanceOf(UserProfileService.class);
        RegistrationService registrationService = instanceOf(RegistrationService.class);

        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setEmail("test@test.com");
        registrationRequest.setPassword("xxx");
        registrationService.newRegistration(registrationRequest);

        registrationService.confirmRegistration("xxx1");

        List<UserProfile> allProfiles = userProfileService.getAllProfiles();
        assertEquals("one profile should be saved", 1, allProfiles.size());

        UserProfile userProfile = allProfiles.get(0);

        IcoService icoService = instanceOf(IcoService.class);
        assertNotNull("ico info should not be null", icoService.getIcoInfo());

        assertEquals("should start with pre ico 100% bonus",100,
                icoService.getIcoInfo().getCurrentStage().getBonusPercent());


        //----------------Proceed to ICO stage---------------
        {
            PurchaseTransaction transaction = new PurchaseTransaction();
            transaction.setSuccess(true);
            BigInteger amount = BigInteger.valueOf(10_000_000).divide(IcoInfo.BASE_TOKEN_PRICE)
                    .multiply(IcoInfo.WEI_MULTIPLIER);
            transaction.setWeiAmount(amount);
            transaction.setAmount(amount);
            transaction.setType(PurchaseTransaction.Type.ETH);
            icoService.makePurchase(userProfile, transaction);

            int currentBonusPercent = icoService.getIcoInfo().getCurrentStage().getBonusPercent();
            assertEquals("should move to next stage", 30, currentBonusPercent);

            //check ico start time is correctly set
            LocalDateTime icoStartTime = icoService.getIcoInfo().getIcoStartTime();

            LocalDateTime now = LocalDateTime.now();
            assertNotNull(icoStartTime);
            assertTrue(icoStartTime.isBefore(now));
            assertTrue(icoStartTime.isAfter(now.minusSeconds(10)));

            //check that tokens were assigned to user
            userProfile = userProfileService.findById(userProfile.getId()).get();

            assertEquals(amount, userProfile.getEthInvested());

            BigInteger tokensBought = userProfile.getTokensBought();
            assertEquals("should set bought tokens to user",
                    BigInteger.valueOf(10_000_000).multiply(IcoInfo.TOKEN_MULTIPLIER), tokensBought);

            BigInteger bonusTokens = userProfile.getBonusTokens();
            assertEquals("should set bonus tokens to user",
                    BigInteger.valueOf(10_000_000).multiply(IcoInfo.TOKEN_MULTIPLIER), bonusTokens);
        }

        //-----------------Proceed to second ICO stage-------------------------
        {
            PurchaseTransaction firstStageTransaction = new PurchaseTransaction();
            firstStageTransaction.setSuccess(true);
            BigInteger amount = BigInteger.valueOf(10_000_000).divide(IcoInfo.BASE_TOKEN_PRICE)
                    .multiply(IcoInfo.WEI_MULTIPLIER);
            firstStageTransaction.setAmount(amount);
            firstStageTransaction.setWeiAmount(amount);
            firstStageTransaction.setType(PurchaseTransaction.Type.ETH);
            icoService.makePurchase(userProfile, firstStageTransaction);

            IcoInfo icoInfo = icoService.getIcoInfo();
            int bonuses = icoInfo.getCurrentStage().getBonusPercent();
            assertEquals("should proceed to next stage", 20, bonuses);
        }

    }

}
