package model;

import org.junit.Test;

import java.math.BigInteger;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class IcoInfoTest {

    @Test
    public void testIsNotActiveBeforePreIco() {
        IcoInfo icoInfo = new IcoInfo();
        icoInfo.setPreIcoStartTime(LocalDateTime.now().plusHours(1));
        try {
            icoInfo.assertIcoIsActive();
            fail("assertion is not thrown");
        } catch (IllegalStateException ex) {
            assertEquals("Pre-ICO is not started" , ex.getMessage());
        }
    }

    @Test
    public void testIsActiveOnIco() {
        IcoInfo icoInfo = new IcoInfo();
        icoInfo.setPreIcoStartTime(LocalDateTime.now().minusHours(1));
        try {
            icoInfo.assertIcoIsActive();
        } catch (IllegalStateException ex) {
            fail("exception should not be thrown (" + ex.getMessage() + ")");
        }
    }

    @Test
    public void getIcoStartTime() {
        IcoInfo icoInfo = new IcoInfo();
        LocalDateTime now = LocalDateTime.now();
        icoInfo.setPreIcoStartTime(now.minusHours(1));
        assertEquals("ico start time should 30 days after " +
                "pre ico start time", now.plusDays(30).minusHours(1), icoInfo.getIcoStartTime());
    }

    @Test
    public void testGetIcoEndTime() {
        IcoInfo icoInfo = new IcoInfo();
        LocalDateTime now = LocalDateTime.now();
        icoInfo.setPreIcoStartTime(now.minusHours(1));
        LocalDateTime icoStartTime = icoInfo.getIcoStartTime();
        assertEquals("ico end time should 60 days after " +
                "start of ico", icoStartTime.plusDays(60), icoInfo.getIcoEndTime());
    }

    @Test
    public void isNotActiveAfterSoftcapReachedAfterTimeout() {
        IcoInfo icoInfo = new IcoInfo();
        LocalDateTime now = LocalDateTime.now();
        icoInfo.setPreIcoStartTime(now.minusDays(60));
        icoInfo.setSoldTokens(IcoInfo.SOFTCAP);
        icoInfo.setSoftcapReachedAt(now.minusHours(IcoInfo.SOFTCAP_TIMEOUT).minusHours(169));
        try {
            icoInfo.assertIcoIsActive();
            fail("exception should be thrown");
        } catch (IllegalStateException ex) {
            assertEquals("ICO is ended", ex.getMessage());
        }
    }

    @Test
    public void isActiveAfterSoftcapReached() {
        IcoInfo icoInfo = new IcoInfo();
        LocalDateTime now = LocalDateTime.now();
        icoInfo.setPreIcoStartTime(now.minusDays(60));
        icoInfo.setSoldTokens(IcoInfo.SOFTCAP);
        icoInfo.setSoftcapReachedAt(now.minusHours(IcoInfo.SOFTCAP_TIMEOUT).plusHours(1));
        try {
            icoInfo.assertIcoIsActive();
        } catch (IllegalStateException ex) {
            fail("exception should not be thrown: " + ex.getMessage());
        }
    }

    @Test
    public void isNotActiveAfterHardcapIsReached() {
        IcoInfo icoInfo = new IcoInfo();
        LocalDateTime now = LocalDateTime.now();
        icoInfo.setPreIcoStartTime(now.minusDays(60));
        icoInfo.setSoftcapReachedAt(now.minusHours(1));
        icoInfo.setSoldTokens(IcoInfo.HARDCAP);
        try {
            icoInfo.assertIcoIsActive();
            fail("exception should be thrown");
        } catch (IllegalStateException ex) {
            assertEquals("hardcap is reached", ex.getMessage());
        }
    }

    @Test
    public void isNotActiveAfterEndTime() {
        IcoInfo icoInfo = new IcoInfo();
        LocalDateTime now = LocalDateTime.now();
        icoInfo.setPreIcoStartTime(now.minusDays(91));
        try {
            icoInfo.assertIcoIsActive();
            fail("exception should be thrown");
        } catch (IllegalStateException ex) {
            assertEquals("ICO is ended", ex.getMessage());
        }
    }

    @Test
    public void testPreIcoBonus() {
        IcoInfo icoInfo = new IcoInfo();
        icoInfo.setSoldTokens(BigInteger.TEN);

        LocalDateTime now = LocalDateTime.now();
        icoInfo.setPreIcoStartTime(now.minusDays(1));

        Stage currentStage = icoInfo.getCurrentStage();
        assertEquals("should return 100 bonus percent",100, currentStage.getBonusPercent());
    }

    @Test
    public void testIsFirstStageWhenPreIcoCapNotReachedAnd30DaysAfterStart() {
        IcoInfo icoInfo = new IcoInfo();

        LocalDateTime now = LocalDateTime.now();
        icoInfo.setPreIcoStartTime(now.minusDays(31));
        icoInfo.setSoldTokens(BigInteger.TEN);
        icoInfo.setIcoStartTime(now.minusHours(1));

        Stage currentStage = icoInfo.getCurrentStage();
        assertEquals("should return 30 bonus percent",30, currentStage.getBonusPercent());
    }

    @Test
    public void testReachingSecondStage() {
        IcoInfo icoInfo = new IcoInfo();
        LocalDateTime now = LocalDateTime.now();
        icoInfo.setPreIcoStartTime(now.minusDays(31));
        icoInfo.setSoldDuringPreIco(BigInteger.valueOf(20_000_000).multiply(IcoInfo.TOKEN_MULTIPLIER));

        BigInteger secondStageTokens = icoInfo.getSoldDuringPreIco()
                .add(BigInteger.valueOf(13_000_000).multiply(IcoInfo.TOKEN_MULTIPLIER))
                .add(BigInteger.ONE);

        icoInfo.setSoldTokens(secondStageTokens);

        Stage currentStage = icoInfo.getCurrentStage();
        assertEquals("should return 20 bonus percent",20, currentStage.getBonusPercent());
    }

    @Test
    public void testFirstStageWithReachingPreIcoCap() {
        IcoInfo icoInfo = new IcoInfo();
        LocalDateTime now = LocalDateTime.now();
        icoInfo.setPreIcoStartTime(now.minusDays(10));

        BigInteger preIcoCap = BigInteger.valueOf(30_000_000).multiply(IcoInfo.TOKEN_MULTIPLIER);
        icoInfo.setIcoStartTime(now.minusHours(1));
        icoInfo.setSoldTokens(preIcoCap);
        icoInfo.setSoldDuringPreIco(preIcoCap);
        icoInfo.setSoftcapReachedAt(now.minusHours(2));

        Stage currentStage = icoInfo.getCurrentStage();
        assertEquals("should return 30 bonus percent",30, currentStage.getBonusPercent());
    }

    @Test
    public void testSecondStageWithNotReachingPreIcoCap() {
        IcoInfo icoInfo = new IcoInfo();
        LocalDateTime now = LocalDateTime.now();
        icoInfo.setPreIcoStartTime(now.minusDays(10));
        icoInfo.setIcoStartTime(now.minusHours(1));

        BigInteger soldDuringPreIco = BigInteger.valueOf(10_000_000).multiply(IcoInfo.TOKEN_MULTIPLIER);
        icoInfo.setSoldDuringPreIco(soldDuringPreIco);

        icoInfo.setSoldTokens(soldDuringPreIco.add(BigInteger.valueOf(13_000_001).multiply(IcoInfo.TOKEN_MULTIPLIER)));

        Stage currentStage = icoInfo.getCurrentStage();
        assertEquals("should return 20 bonus percent",20, currentStage.getBonusPercent());
    }

}