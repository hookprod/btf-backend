package model;

import org.junit.Test;
import org.mindrot.jbcrypt.BCrypt;
import static org.junit.Assert.*;

/**
 * Created by Gleb Zykov on 01/02/2018.
 */
public class UserProfileTest {

    @Test
    public void testPasswordHashing() throws Exception {
        String passwordHash = BCrypt.hashpw("12345", BCrypt.gensalt());

        boolean valid = BCrypt.checkpw("12345", passwordHash);

        assertTrue("correct password should return true", valid);

        boolean invalid = BCrypt.checkpw("1234", passwordHash);
        assertFalse("incorrect password shoudl return false", invalid);
    }

}