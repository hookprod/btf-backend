name := """BITRUST-backend"""
organization := "bitrust"

version := "1.0"

PlayKeys.devSettings := Seq("play.server.http.port" -> "8000")

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.2"

libraryDependencies += guice
libraryDependencies += javaJpa
PlayKeys.externalizeResources := false

libraryDependencies += "org.hibernate" % "hibernate-entitymanager" % "5.2.6.Final"
libraryDependencies += "dom4j" % "dom4j" % "1.6.1"
libraryDependencies += "org.codehaus.janino" % "janino" % "2.6.1"
libraryDependencies += "org.hibernate" % "hibernate-java8" % "5.2.6.Final"

libraryDependencies += "javax.el" % "javax.el-api" % "3.0.0"
libraryDependencies += "org.glassfish.web" % "el-impl" % "2.2"

libraryDependencies += "org.mindrot" % "jbcrypt" % "0.3m"

libraryDependencies += "io.jsonwebtoken" % "jjwt" % "0.9.0"

//for bitcoin
//libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.25"
libraryDependencies += "com.github.briandilley.jsonrpc4j" % "jsonrpc4j" % "1.5.0"
libraryDependencies += "org.web3j" % "core" % "3.1.1"

libraryDependencies += "com.opencsv" % "opencsv" % "4.0"

//retrofit
libraryDependencies += "com.squareup.retrofit2" % "retrofit" % "2.1.0"
libraryDependencies += "com.squareup.retrofit2" % "converter-gson" % "2.1.0"
libraryDependencies += "com.squareup.okhttp3" % "logging-interceptor" % "3.5.0"

libraryDependencies += "com.github.kevinsawicki" % "http-request" % "6.0"

libraryDependencies += "javax.mail" % "mail" % "1.4.7"

libraryDependencies += "com.h2database" % "h2" % "1.4.196" % "test"

libraryDependencies += "org.mockito" % "mockito-core" % "1.9.5" % "test"

javaOptions in Test += "-Dconfig.resource=test.conf"

