/**
 * Created by glebzykov on 25/07/2017.
 */

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        if (--timer < 0) {
            // timer = duration;
            display.text('Rate expired. Reload page to update it');
        } else {
            display.text('Rate is frozen for ' +  minutes + ":" + seconds);
        }
    }, 1000);
}

display = $('#remainingTime');
startTimer(remainingTime, display);
